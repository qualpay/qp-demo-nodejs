let QualpayPGApi = require('qualpay_payment_gateway_api');
const config = require('./../../config/config.json');

/**
 * Post a sale request to Payment Gateway
 * Use card id returned from embedded fields in the request
 */
exports.post_payment = (req, res) => {
  let defaultClient = QualpayPGApi.ApiClient.instance;
  // Configure HTTP basic authorization: basicAuth
  defaultClient.basePath = config.qualpay_api_base_path + '/pg';
  defaultClient.authentications.basicAuth.password = config.qualpay_api_security_key;
  let api = new QualpayPGApi.PaymentGatewayApi();

  body = {
    amt_tran: req.body.amt_tran,
    merchant_id: config.qualpay_merchant_id,
    card_id: req.body.card_id
  };

  api.sale(body, (err, response) => {
    res.set('Content-Type', 'application/json');
    if (err) {
      console.error(err.error);
      res.send(err.error);
    } else {
      let saleResp = {
        pgId: response.pg_id,
        respCode: response.rcode,
        authCode: response.auth_code,
        authAvsResult: response.auth_avs_result,
        msg: response.rmsg
      };
      res.send(saleResp);
    }
  });
};
