let QualpayPlatformApi = require('qualpay_platform_api');
const config = require('./../../config/config.json');

/**
 * Get embedded transient key.
 */
exports.get_transient_key = (req, res) => {
  let defaultClient = QualpayPlatformApi.ApiClient.instance;
  // Configure HTTP basic authorization: basicAuth
  defaultClient.basePath = config.qualpay_api_base_path + '/platform';
  defaultClient.authentications.basicAuth.password = config.qualpay_api_security_key;
  let api = new QualpayPlatformApi.EmbeddedFieldsApi();

  //Get a transient key for use with embedded fields
  api.getEmbeddedTransientKey((error, response) => {
    res.set('Content-Type', 'application/json');
    if (error) {
      console.error(error);
      res.send(error);
    } else {
      let embeddedResp = {
        message: response.message,
        code: response.code,
        transient_key: response.data.transient_key,
        merchant_id: response.data.merchant_id,
        expiry_time: response.data.expiry_time
      };
      res.send(embeddedResp);
    }
  });
};
