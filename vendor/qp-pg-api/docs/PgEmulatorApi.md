# QualpayPaymentGatewayApi.PgEmulatorApi

All URIs are relative to *https://api-test.qualpay.com:443/pg*

Method | HTTP request | Description
------------- | ------------- | -------------
[**emulatorHandlerUsingPOST**](PgEmulatorApi.md#emulatorHandlerUsingPOST) | **POST** /emulator/xml/v1/request.api | emulatorHandler


<a name="emulatorHandlerUsingPOST"></a>
# **emulatorHandlerUsingPOST**
> &#39;String&#39; emulatorHandlerUsingPOST()

emulatorHandler

### Example
```javascript
var QualpayPaymentGatewayApi = require('qualpay_payment_gateway_api');
var defaultClient = QualpayPaymentGatewayApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPaymentGatewayApi.PgEmulatorApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.emulatorHandlerUsingPOST(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

**&#39;String&#39;**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/xml, application/json, text/xml
 - **Accept**: application/json, */*, text/xml

