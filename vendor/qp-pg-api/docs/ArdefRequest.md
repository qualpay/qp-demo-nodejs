# QualpayPaymentGatewayApi.ArdefRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cardNumber** | **String** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 19 N&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;Cardholder&#39;s card number. | 
**merchantId** | **Number** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 12 N&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;Unique identifier on the Qualpay system. | 


