# QualpayPaymentGatewayApi.PGApiEmailReceiptRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**merchantId** | **Number** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 12 N&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;Unique identifier on the Qualpay system. | 
**developerId** | **String** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 16 AN&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;Used to indicate which company developed the integration to the Qualpay Payment Gateway. This may also be used to provide the payment solution name that is connected to the Qualpay Payment Gateway. | [optional] 
**emailAddress** | **[String]** |  AN&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;An array of email addresses to which the transaction receipt should be sent to.  | 
**logoUrl** | **String** |  AN&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;A link to the logo image that will be included in the transaction receipt.  | [optional] 
**vendorId** | **Number** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 12 N&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;Identifies the vendor to which this email receipt request applies. | [optional] 


