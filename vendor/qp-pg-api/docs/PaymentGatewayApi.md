# QualpayPaymentGatewayApi.PaymentGatewayApi

All URIs are relative to *https://api-test.qualpay.com:443/pg*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authorization**](PaymentGatewayApi.md#authorization) | **POST** /auth | Authorize transaction
[**batchClose**](PaymentGatewayApi.md#batchClose) | **POST** /batchClose | Close batch
[**callVoid**](PaymentGatewayApi.md#callVoid) | **POST** /void/{pgIdOrig} | Void previously authorized transaction
[**capture**](PaymentGatewayApi.md#capture) | **POST** /capture/{pgIdOrig} | Capture authorized transaction
[**credit**](PaymentGatewayApi.md#credit) | **POST** /credit | Issue credit to cardholder
[**force**](PaymentGatewayApi.md#force) | **POST** /force | Force transaction approval
[**getCardTypeInformation_**](PaymentGatewayApi.md#getCardTypeInformation_) | **POST** /ardef | Card Type Information
[**refund**](PaymentGatewayApi.md#refund) | **POST** /refund/{pgIdOrig} | Refund previously captured transaction
[**sale**](PaymentGatewayApi.md#sale) | **POST** /sale | Sale (Auth + Capture)
[**sendReceipt**](PaymentGatewayApi.md#sendReceipt) | **POST** /emailReceipt/{pgId} | Send transaction receipt email
[**tokenize**](PaymentGatewayApi.md#tokenize) | **POST** /tokenize | Tokenize card
[**verify**](PaymentGatewayApi.md#verify) | **POST** /verify | Verify Card


<a name="authorization"></a>
# **authorization**
> PGApiTransactionResponse authorization(body)

Authorize transaction

An approved transaction will continue to be open until it expires or a capture message is received. Authorizations are automatically voided if they are not captured within 28 days, although most issuing banks will release the hold after 24 hours in retail environments or 7 days in card not present environments.

### Example
```javascript
var QualpayPaymentGatewayApi = require('qualpay_payment_gateway_api');
var defaultClient = QualpayPaymentGatewayApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPaymentGatewayApi.PaymentGatewayApi();

var body = new QualpayPaymentGatewayApi.PGApiTransactionRequest(); // PGApiTransactionRequest | Payment Gateway Authorization Request


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.authorization(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PGApiTransactionRequest**](PGApiTransactionRequest.md)| Payment Gateway Authorization Request | 

### Return type

[**PGApiTransactionResponse**](PGApiTransactionResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="batchClose"></a>
# **batchClose**
> PGApiBatchCloseResponse batchClose(body)

Close batch

This message is used when the timing of the batch close needs to be controlled by the merchant rather than relying on the daily automatic batch close.

### Example
```javascript
var QualpayPaymentGatewayApi = require('qualpay_payment_gateway_api');
var defaultClient = QualpayPaymentGatewayApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPaymentGatewayApi.PaymentGatewayApi();

var body = new QualpayPaymentGatewayApi.PGApiBatchCloseRequest(); // PGApiBatchCloseRequest | Payment Gateway Batch Close Request


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.batchClose(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PGApiBatchCloseRequest**](PGApiBatchCloseRequest.md)| Payment Gateway Batch Close Request | 

### Return type

[**PGApiBatchCloseResponse**](PGApiBatchCloseResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="callVoid"></a>
# **callVoid**
> PGApiVoidResponse callVoid(pgIdOrig, body)

Void previously authorized transaction

Authorizations can be voided at any time. Captured transactions can be voided until the batch is closed.

### Example
```javascript
var QualpayPaymentGatewayApi = require('qualpay_payment_gateway_api');
var defaultClient = QualpayPaymentGatewayApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPaymentGatewayApi.PaymentGatewayApi();

var pgIdOrig = "pgIdOrig_example"; // String | pgIdOrig

var body = new QualpayPaymentGatewayApi.PGApiVoidRequest(); // PGApiVoidRequest | Payment Gateway Void Request


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.callVoid(pgIdOrig, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pgIdOrig** | **String**| pgIdOrig | 
 **body** | [**PGApiVoidRequest**](PGApiVoidRequest.md)| Payment Gateway Void Request | 

### Return type

[**PGApiVoidResponse**](PGApiVoidResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="capture"></a>
# **capture**
> PGApiCaptureResponse capture(pgIdOrig, body)

Capture authorized transaction

A capture message may be completed for any amount up to the originally authorized amount.

### Example
```javascript
var QualpayPaymentGatewayApi = require('qualpay_payment_gateway_api');
var defaultClient = QualpayPaymentGatewayApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPaymentGatewayApi.PaymentGatewayApi();

var pgIdOrig = "pgIdOrig_example"; // String | pgIdOrig

var body = new QualpayPaymentGatewayApi.PGApiCaptureRequest(); // PGApiCaptureRequest | Payment Gateway Capture Request


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.capture(pgIdOrig, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pgIdOrig** | **String**| pgIdOrig | 
 **body** | [**PGApiCaptureRequest**](PGApiCaptureRequest.md)| Payment Gateway Capture Request | 

### Return type

[**PGApiCaptureResponse**](PGApiCaptureResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="credit"></a>
# **credit**
> PGApiTransactionResponse credit(body)

Issue credit to cardholder

A non-referenced credit requires that the cardholder data be provided in the message. This message is only available during the first 30 days of account production activity - the refund message should generally be used to return money to the cardholder.

### Example
```javascript
var QualpayPaymentGatewayApi = require('qualpay_payment_gateway_api');
var defaultClient = QualpayPaymentGatewayApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPaymentGatewayApi.PaymentGatewayApi();

var body = new QualpayPaymentGatewayApi.PGApiTransactionRequest(); // PGApiTransactionRequest | Payment Gateway Credit Request


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.credit(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PGApiTransactionRequest**](PGApiTransactionRequest.md)| Payment Gateway Credit Request | 

### Return type

[**PGApiTransactionResponse**](PGApiTransactionResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="force"></a>
# **force**
> PGApiTransactionResponse force(body)

Force transaction approval

Can be used when an online authorization request received a &#39;declined&#39; reason code and the merchant received an authorization from a voice or automated response (ARU) system. The required fields are the same as a sale or authorization request, except that the expiration date (exp_date) is not required, and the 6-character authorization code (auth_code) IS required.

### Example
```javascript
var QualpayPaymentGatewayApi = require('qualpay_payment_gateway_api');
var defaultClient = QualpayPaymentGatewayApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPaymentGatewayApi.PaymentGatewayApi();

var body = new QualpayPaymentGatewayApi.PGApiTransactionRequest(); // PGApiTransactionRequest | Payment Gateway Force Request


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.force(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PGApiTransactionRequest**](PGApiTransactionRequest.md)| Payment Gateway Force Request | 

### Return type

[**PGApiTransactionResponse**](PGApiTransactionResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCardTypeInformation_"></a>
# **getCardTypeInformation_**
> ArdefResponse getCardTypeInformation_(body)

Card Type Information

Get Card Type Information for Visa, Mastercard and Discover.

### Example
```javascript
var QualpayPaymentGatewayApi = require('qualpay_payment_gateway_api');
var defaultClient = QualpayPaymentGatewayApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPaymentGatewayApi.PaymentGatewayApi();

var body = new QualpayPaymentGatewayApi.ArdefRequest(); // ArdefRequest | Card Type Request


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getCardTypeInformation_(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ArdefRequest**](ArdefRequest.md)| Card Type Request | 

### Return type

[**ArdefResponse**](ArdefResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="refund"></a>
# **refund**
> PGApiRefundResponse refund(pgIdOrig, body)

Refund previously captured transaction

Returns money to the cardholder from a previously captured transaction. Multiple refunds are allowed per captured transaction, provided that the sum of all refunds does not exceed the original captured transaction amount. Authorizations that have not been captured are not eligible for refund.

### Example
```javascript
var QualpayPaymentGatewayApi = require('qualpay_payment_gateway_api');
var defaultClient = QualpayPaymentGatewayApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPaymentGatewayApi.PaymentGatewayApi();

var pgIdOrig = "pgIdOrig_example"; // String | pgIdOrig

var body = new QualpayPaymentGatewayApi.PGApiRefundRequest(); // PGApiRefundRequest | Payment Gateway Refund Request


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.refund(pgIdOrig, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pgIdOrig** | **String**| pgIdOrig | 
 **body** | [**PGApiRefundRequest**](PGApiRefundRequest.md)| Payment Gateway Refund Request | 

### Return type

[**PGApiRefundResponse**](PGApiRefundResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="sale"></a>
# **sale**
> PGApiTransactionResponse sale(body)

Sale (Auth + Capture)

This message will perform an authorization of the transaction, and if approved will immediately capture the transaction to be included in the next batch close. It is used in card present environments, and also card not present environments where no physical goods are being shipped.

### Example
```javascript
var QualpayPaymentGatewayApi = require('qualpay_payment_gateway_api');
var defaultClient = QualpayPaymentGatewayApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPaymentGatewayApi.PaymentGatewayApi();

var body = new QualpayPaymentGatewayApi.PGApiTransactionRequest(); // PGApiTransactionRequest | Payment Gateway Sale Request


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.sale(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PGApiTransactionRequest**](PGApiTransactionRequest.md)| Payment Gateway Sale Request | 

### Return type

[**PGApiTransactionResponse**](PGApiTransactionResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="sendReceipt"></a>
# **sendReceipt**
> PGApiEmailReceiptResponse sendReceipt(pgId, body)

Send transaction receipt email

Sent the transaction receipt to multiple email addresses. Receipts can be sent only for successful transactions.

### Example
```javascript
var QualpayPaymentGatewayApi = require('qualpay_payment_gateway_api');
var defaultClient = QualpayPaymentGatewayApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPaymentGatewayApi.PaymentGatewayApi();

var pgId = "pgId_example"; // String | pgId

var body = new QualpayPaymentGatewayApi.PGApiEmailReceiptRequest(); // PGApiEmailReceiptRequest | Payment Gateway Email Receipt Request


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.sendReceipt(pgId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pgId** | **String**| pgId | 
 **body** | [**PGApiEmailReceiptRequest**](PGApiEmailReceiptRequest.md)| Payment Gateway Email Receipt Request | 

### Return type

[**PGApiEmailReceiptResponse**](PGApiEmailReceiptResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="tokenize"></a>
# **tokenize**
> PGApiTokenizeResponse tokenize(body)

Tokenize card

Once stored, a unique card identifier is returned for use in future transactions. Optionally, tokenization can be requested in an auth, verify, force, credit, or sale message by sending the tokenize field set to true.

### Example
```javascript
var QualpayPaymentGatewayApi = require('qualpay_payment_gateway_api');
var defaultClient = QualpayPaymentGatewayApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPaymentGatewayApi.PaymentGatewayApi();

var body = new QualpayPaymentGatewayApi.PGApiTokenizeRequest(); // PGApiTokenizeRequest | Payment Gateway Tokenize Request


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.tokenize(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PGApiTokenizeRequest**](PGApiTokenizeRequest.md)| Payment Gateway Tokenize Request | 

### Return type

[**PGApiTokenizeResponse**](PGApiTokenizeResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="verify"></a>
# **verify**
> PGApiVerifyResponse verify(body)

Verify Card

A verify message will return success if the cardholder information was verified by the issuer. If AVS or CVV data is included in the message, then the AVS or CVV result code will be returned in the response message.

### Example
```javascript
var QualpayPaymentGatewayApi = require('qualpay_payment_gateway_api');
var defaultClient = QualpayPaymentGatewayApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPaymentGatewayApi.PaymentGatewayApi();

var body = new QualpayPaymentGatewayApi.PGApiVerifyRequest(); // PGApiVerifyRequest | Payment Gateway Card Verify Request


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.verify(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PGApiVerifyRequest**](PGApiVerifyRequest.md)| Payment Gateway Card Verify Request | 

### Return type

[**PGApiVerifyResponse**](PGApiVerifyResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

