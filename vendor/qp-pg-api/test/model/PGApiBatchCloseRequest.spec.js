/**
 * Qualpay Payment Gateway API
 * This document describes the Qualpay Payment Gateway API.
 *
 * OpenAPI spec version: 1.7
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.QualpayPaymentGatewayApi);
  }
}(this, function(expect, QualpayPaymentGatewayApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new QualpayPaymentGatewayApi.PGApiBatchCloseRequest();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('PGApiBatchCloseRequest', function() {
    it('should create an instance of PGApiBatchCloseRequest', function() {
      // uncomment below and update the code to test PGApiBatchCloseRequest
      //var instance = new QualpayPaymentGatewayApi.PGApiBatchCloseRequest();
      //expect(instance).to.be.a(QualpayPaymentGatewayApi.PGApiBatchCloseRequest);
    });

    it('should have the property merchantId (base name: "merchant_id")', function() {
      // uncomment below and update the code to test the property merchantId
      //var instance = new QualpayPaymentGatewayApi.PGApiBatchCloseRequest();
      //expect(instance).to.be();
    });

    it('should have the property developerId (base name: "developer_id")', function() {
      // uncomment below and update the code to test the property developerId
      //var instance = new QualpayPaymentGatewayApi.PGApiBatchCloseRequest();
      //expect(instance).to.be();
    });

    it('should have the property echoFields (base name: "echo_fields")', function() {
      // uncomment below and update the code to test the property echoFields
      //var instance = new QualpayPaymentGatewayApi.PGApiBatchCloseRequest();
      //expect(instance).to.be();
    });

    it('should have the property locId (base name: "loc_id")', function() {
      // uncomment below and update the code to test the property locId
      //var instance = new QualpayPaymentGatewayApi.PGApiBatchCloseRequest();
      //expect(instance).to.be();
    });

    it('should have the property profileId (base name: "profile_id")', function() {
      // uncomment below and update the code to test the property profileId
      //var instance = new QualpayPaymentGatewayApi.PGApiBatchCloseRequest();
      //expect(instance).to.be();
    });

    it('should have the property reportData (base name: "report_data")', function() {
      // uncomment below and update the code to test the property reportData
      //var instance = new QualpayPaymentGatewayApi.PGApiBatchCloseRequest();
      //expect(instance).to.be();
    });

    it('should have the property retryId (base name: "retry_id")', function() {
      // uncomment below and update the code to test the property retryId
      //var instance = new QualpayPaymentGatewayApi.PGApiBatchCloseRequest();
      //expect(instance).to.be();
    });

    it('should have the property sessionId (base name: "session_id")', function() {
      // uncomment below and update the code to test the property sessionId
      //var instance = new QualpayPaymentGatewayApi.PGApiBatchCloseRequest();
      //expect(instance).to.be();
    });

    it('should have the property tranCurrency (base name: "tran_currency")', function() {
      // uncomment below and update the code to test the property tranCurrency
      //var instance = new QualpayPaymentGatewayApi.PGApiBatchCloseRequest();
      //expect(instance).to.be();
    });

    it('should have the property userId (base name: "user_id")', function() {
      // uncomment below and update the code to test the property userId
      //var instance = new QualpayPaymentGatewayApi.PGApiBatchCloseRequest();
      //expect(instance).to.be();
    });

  });

}));
