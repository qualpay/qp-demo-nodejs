# QualpayPlatformApi.CustomerVaultApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addBillingCard**](CustomerVaultApi.md#addBillingCard) | **POST** /vault/customer/{customer_id}/billing | Add a Billing Card
[**addCustomer**](CustomerVaultApi.md#addCustomer) | **POST** /vault/customer | Add a Customer
[**addShippingAddress**](CustomerVaultApi.md#addShippingAddress) | **POST** /vault/customer/{customer_id}/shipping | Add a Shipping Address
[**browseCustomers**](CustomerVaultApi.md#browseCustomers) | **GET** /vault/customer | Get all Customers
[**deleteBillingCard**](CustomerVaultApi.md#deleteBillingCard) | **PUT** /vault/customer/{customer_id}/billing/delete | Delete a Billing Card
[**deleteCustomer**](CustomerVaultApi.md#deleteCustomer) | **DELETE** /vault/customer/{customer_id} | Delete a Customer
[**deleteShippingAddress**](CustomerVaultApi.md#deleteShippingAddress) | **DELETE** /vault/customer/{customer_id}/shipping/{id} | Delete a Shipping Address
[**getBillingCards**](CustomerVaultApi.md#getBillingCards) | **GET** /vault/customer/{customer_id}/billing | Get Billing Cards
[**getCustomer**](CustomerVaultApi.md#getCustomer) | **GET** /vault/customer/{customer_id} | Get by Customer ID
[**getShippingAddresses**](CustomerVaultApi.md#getShippingAddresses) | **GET** /vault/customer/{customer_id}/shipping | Get Shipping Addresses
[**setPrimaryBillingCard**](CustomerVaultApi.md#setPrimaryBillingCard) | **PUT** /vault/customer/{customer_id}/billing/primary | Set Primary Billing Card
[**setPrimaryShippingAddress**](CustomerVaultApi.md#setPrimaryShippingAddress) | **PUT** /vault/customer/{customer_id}/shipping/primary | Set Primary Shipping Address
[**updateBillingCard**](CustomerVaultApi.md#updateBillingCard) | **PUT** /vault/customer/{customer_id}/billing | Update a Billing Card
[**updateCustomer**](CustomerVaultApi.md#updateCustomer) | **PUT** /vault/customer/{customer_id} | Update a Customer
[**updateShippingAddress**](CustomerVaultApi.md#updateShippingAddress) | **PUT** /vault/customer/{customer_id}/shipping | Update a Shipping Address


<a name="addBillingCard"></a>
# **addBillingCard**
> CustomerResponse addBillingCard(customerId, body)

Add a Billing Card

Tokenizes and adds a billing card to a customer record. The response will include the tokenized card_number in the field, card_id, which can be used to manage the card and can be used in place of the card_number in Payment Gateway API requests. There is no limit on the number of billing cards you can add to a customer. The first billing card added will be designated as the primary billing card, used for subscription payments. You can use the Set Primary Billing Card API to change the default billing card.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var customerId = "customerId_example"; // String | Customer ID

var body = new QualpayPlatformApi.AddBillingCardRequest(); // AddBillingCardRequest | Customer


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.addBillingCard(customerId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **String**| Customer ID | 
 **body** | [**AddBillingCardRequest**](AddBillingCardRequest.md)| Customer | 

### Return type

[**CustomerResponse**](CustomerResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="addCustomer"></a>
# **addCustomer**
> CustomerResponse addCustomer(body)

Add a Customer

Adds a customer to the Qualpay Customer Vault. A customer_id is required to create invoice and subscription payments. Billing cards, billing addresses and shipping addresses may be included in this request.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var body = new QualpayPlatformApi.AddCustomerRequest(); // AddCustomerRequest | Customer


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.addCustomer(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AddCustomerRequest**](AddCustomerRequest.md)| Customer | 

### Return type

[**CustomerResponse**](CustomerResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="addShippingAddress"></a>
# **addShippingAddress**
> CustomerResponse addShippingAddress(customerId, body)

Add a Shipping Address

Adds a shipping address to a customer. The response will include a unique shipping_id. The shipping_id can be used to manage the shipping_address.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var customerId = "customerId_example"; // String | Shipping Address

var body = new QualpayPlatformApi.AddShippingAddressRequest(); // AddShippingAddressRequest | Shipping Address


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.addShippingAddress(customerId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **String**| Shipping Address | 
 **body** | [**AddShippingAddressRequest**](AddShippingAddressRequest.md)| Shipping Address | 

### Return type

[**CustomerResponse**](CustomerResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="browseCustomers"></a>
# **browseCustomers**
> CustomerListResponse browseCustomers(opts)

Get all Customers

Gets an array of customer vault objects. You can use filters to limit the results returned. Optional query parameters determine size and sort order of the returned array.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "customer_id", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "desc", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example", // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
  'merchantId': 0 // Number | Unique ID assigned by Qualpay to a merchant.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseCustomers(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to customer_id]
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 
 **merchantId** | **Number**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**CustomerListResponse**](CustomerListResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteBillingCard"></a>
# **deleteBillingCard**
> CustomerResponse deleteBillingCard(customerId, body)

Delete a Billing Card

Deletes a customer&#39;s specific billing card based on the card_id provided in the request. The card_id is required in the request body to delete the billing card. A billing card with active subscriptions cannot be deleted. 

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var customerId = "customerId_example"; // String | Customer ID

var body = new QualpayPlatformApi.DeleteBillingCardRequest(); // DeleteBillingCardRequest | Customer


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.deleteBillingCard(customerId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **String**| Customer ID | 
 **body** | [**DeleteBillingCardRequest**](DeleteBillingCardRequest.md)| Customer | 

### Return type

[**CustomerResponse**](CustomerResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteCustomer"></a>
# **deleteCustomer**
> QPApiResponse deleteCustomer(customerId, opts)

Delete a Customer

Deletes a customer from the customer vault. Deleting a customer will delete all the subscriptions associated with the customer. **This operation cannot be reversed.**

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var customerId = "customerId_example"; // String | Customer ID

var opts = { 
  'merchantId': 0 // Number | Unique ID assigned by Qualpay to a merchant.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.deleteCustomer(customerId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **String**| Customer ID | 
 **merchantId** | **Number**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteShippingAddress"></a>
# **deleteShippingAddress**
> CustomerResponse deleteShippingAddress(customerId, id, opts)

Delete a Shipping Address

Deletes a customer&#39;s specific shipping address based on the shipping_id provided in the request.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var customerId = "customerId_example"; // String | Customer ID

var id = 789; // Number | Shipping ID

var opts = { 
  'merchantId': 0 // Number | Unique ID assigned by Qualpay to a merchant.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.deleteShippingAddress(customerId, id, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **String**| Customer ID | 
 **id** | **Number**| Shipping ID | 
 **merchantId** | **Number**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**CustomerResponse**](CustomerResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getBillingCards"></a>
# **getBillingCards**
> GetBillingResponse getBillingCards(customerId, opts)

Get Billing Cards

Gets a list of billing cards associated with a customer_id. The response will contain an array of billing_cards.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var customerId = "customerId_example"; // String | Customer ID

var opts = { 
  'merchantId': 0 // Number | Unique ID assigned by Qualpay to a merchant.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getBillingCards(customerId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **String**| Customer ID | 
 **merchantId** | **Number**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**GetBillingResponse**](GetBillingResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCustomer"></a>
# **getCustomer**
> CustomerResponse getCustomer(customerId, opts)

Get by Customer ID

Gets a customer vault record by customer_id.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var customerId = "customerId_example"; // String | Customer ID

var opts = { 
  'merchantId': 0 // Number | Unique ID assigned by Qualpay to a merchant.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getCustomer(customerId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **String**| Customer ID | 
 **merchantId** | **Number**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**CustomerResponse**](CustomerResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getShippingAddresses"></a>
# **getShippingAddresses**
> GetShippingResponse getShippingAddresses(customerId, opts)

Get Shipping Addresses

Gets all shipping addresses for a customer. The response will include an array of shipping addresses. 

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var customerId = "customerId_example"; // String | Customer ID

var opts = { 
  'merchantId': 0 // Number | Unique ID assigned by Qualpay to a merchant.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getShippingAddresses(customerId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **String**| Customer ID | 
 **merchantId** | **Number**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**GetShippingResponse**](GetShippingResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="setPrimaryBillingCard"></a>
# **setPrimaryBillingCard**
> CustomerResponse setPrimaryBillingCard(customerId, body)

Set Primary Billing Card

Sets a customer&#39;s specific billing card as the primary billing card. A primary billing card is used for subscription payments.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var customerId = "customerId_example"; // String | Customer ID

var body = new QualpayPlatformApi.SetPrimaryBillingCardRequest(); // SetPrimaryBillingCardRequest | Customer


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.setPrimaryBillingCard(customerId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **String**| Customer ID | 
 **body** | [**SetPrimaryBillingCardRequest**](SetPrimaryBillingCardRequest.md)| Customer | 

### Return type

[**CustomerResponse**](CustomerResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="setPrimaryShippingAddress"></a>
# **setPrimaryShippingAddress**
> CustomerResponse setPrimaryShippingAddress(customerId, body)

Set Primary Shipping Address

Sets a customer&#39;s specific shipping address as primary.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var customerId = "customerId_example"; // String | Customer ID

var body = new QualpayPlatformApi.SetPrimaryShippingAddressRequest(); // SetPrimaryShippingAddressRequest | Shipping Address


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.setPrimaryShippingAddress(customerId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **String**| Customer ID | 
 **body** | [**SetPrimaryShippingAddressRequest**](SetPrimaryShippingAddressRequest.md)| Shipping Address | 

### Return type

[**CustomerResponse**](CustomerResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateBillingCard"></a>
# **updateBillingCard**
> CustomerResponse updateBillingCard(customerId, body)

Update a Billing Card

Updates a customer&#39;s billing card by card_id. The complete billing record should be included in the request.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var customerId = "customerId_example"; // String | Customer ID

var body = new QualpayPlatformApi.UpdateBillingCardRequest(); // UpdateBillingCardRequest | Customer


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateBillingCard(customerId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **String**| Customer ID | 
 **body** | [**UpdateBillingCardRequest**](UpdateBillingCardRequest.md)| Customer | 

### Return type

[**CustomerResponse**](CustomerResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateCustomer"></a>
# **updateCustomer**
> CustomerResponse updateCustomer(customerId, body)

Update a Customer

Updates a customer vault record. You can send the entire resource or only the fields that require an update. When updating array fields - billing_cards or shipping_addresses - the entire array should be included in the request body. 

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var customerId = "customerId_example"; // String | Customer ID

var body = new QualpayPlatformApi.UpdateCustomerRequest(); // UpdateCustomerRequest | Customer


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateCustomer(customerId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **String**| Customer ID | 
 **body** | [**UpdateCustomerRequest**](UpdateCustomerRequest.md)| Customer | 

### Return type

[**CustomerResponse**](CustomerResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateShippingAddress"></a>
# **updateShippingAddress**
> CustomerResponse updateShippingAddress(customerId, body)

Update a Shipping Address

Update a shipping address for a customer.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.CustomerVaultApi();

var customerId = "customerId_example"; // String | Shipping Address

var body = new QualpayPlatformApi.UpdateShippingAddressRequest(); // UpdateShippingAddressRequest | Shipping Address


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateShippingAddress(customerId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **String**| Shipping Address | 
 **body** | [**UpdateShippingAddressRequest**](UpdateShippingAddressRequest.md)| Shipping Address | 

### Return type

[**CustomerResponse**](CustomerResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

