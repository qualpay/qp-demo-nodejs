# QualpayPlatformApi.BrowseAppsData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appId** | **Number** | Unique ID assigned by Qualpay to this application. | 
**merchantId** | **Number** | Unique ID assigned by Qualpay to a merchant. | 
**appStatus** | **String** | The current status of the application. | 
**dbaName** | **String** | The &#39;doing business as&#39; name, as it is currently on the merchant application. | 
**dbTimestamp** | **Date** | The timestamp the application was created. | 
**submitTimestamp** | **Date** | The timestamp the application was submitted as complete. | 
**creditTimestamp** | **Date** | The timestamp the application&#39;s credit decision was made. | 


