# QualpayPlatformApi.GetShippingAddressesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shippingAddresses** | [**[ShippingAddress]**](ShippingAddress.md) | &lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;An array of shipping addresses.  | [optional] 


