# QualpayPlatformApi.ApplicationModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appId** | **Number** | Unique ID assigned by Qualpay to this application. | [optional] 
**merchantId** | **Number** | Unique ID assigned by Qualpay to a merchant. | [optional] 
**appStatus** | **String** | The current status of the application. | [optional] 
**dbaName** | **String** | The &#39;doing business as&#39; name, as it is currently on the merchant application. | [optional] 
**dbTimestamp** | **Date** | The timestamp the application was created. | [optional] 
**submitTimestamp** | **Date** | The timestamp the application was submitted as complete. | [optional] 
**creditTimestamp** | **Date** | The timestamp the application&#39;s credit decision was made. | [optional] 


