# QualpayPlatformApi.RotateKeyRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**minutes** | **Number** | The number of minutes the previous key will be valid.  | [optional] 


