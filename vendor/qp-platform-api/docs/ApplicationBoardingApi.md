# QualpayPlatformApi.ApplicationBoardingApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addApp**](ApplicationBoardingApi.md#addApp) | **POST** /application/add | Create Application
[**browseApps**](ApplicationBoardingApi.md#browseApps) | **GET** /application/browse | Browse applications
[**browseSalesReps**](ApplicationBoardingApi.md#browseSalesReps) | **GET** /application/sales/browse | Browse Sales Reps
[**emailApp**](ApplicationBoardingApi.md#emailApp) | **POST** /application/{appId}/email | Email Application
[**getApp**](ApplicationBoardingApi.md#getApp) | **GET** /application/{appId}/get | Get Application
[**saveData**](ApplicationBoardingApi.md#saveData) | **POST** /application/{appId}/data | Save Data
[**uploadFile**](ApplicationBoardingApi.md#uploadFile) | **POST** /application/{appId}/upload | Upload Document
[**validateApp**](ApplicationBoardingApi.md#validateApp) | **GET** /application/{appId}/validate | Validate Application


<a name="addApp"></a>
# **addApp**
> GetAppResult addApp(opts)

Create Application

Creates a new merchant application, returning the new application&#39;s ID as well as the pricing options available.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ApplicationBoardingApi();

var opts = { 
  'body': new QualpayPlatformApi.AddAppRequest() // AddAppRequest | Application Request
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.addApp(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AddAppRequest**](AddAppRequest.md)| Application Request | [optional] 

### Return type

[**GetAppResult**](GetAppResult.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="browseApps"></a>
# **browseApps**
> BrowseAppsResult browseApps(opts)

Browse applications

Retrieves an array of Applications. Optional query parameters determine the page size and sorting of the data.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ApplicationBoardingApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "orderOn_example", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "orderBy_example", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseApps(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] 
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] 
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**BrowseAppsResult**](BrowseAppsResult.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="browseSalesReps"></a>
# **browseSalesReps**
> BrowseSalesRepsResult browseSalesReps(opts)

Browse Sales Reps

Retrieves an array of Users who can be used during the creation of an application to identify the sales representitive. Optional query parameters determine the page size and sorting of the data.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ApplicationBoardingApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "orderOn_example", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "orderBy_example", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseSalesReps(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] 
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] 
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**BrowseSalesRepsResult**](BrowseSalesRepsResult.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="emailApp"></a>
# **emailApp**
> QPApiResponse emailApp(appId)

Email Application

Emails an application to the business contact for click-through agreement. The business_contact_email must be pre-populated before calling this service.  If present, the business_contact_name will be used to adress the recipient in the email.  An application otherwise does not have to pass validation in order to be emailed.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ApplicationBoardingApi();

var appId = 789; // Number | Application ID


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.emailApp(appId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **Number**| Application ID | 

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getApp"></a>
# **getApp**
> GetAppResult getApp(appId)

Get Application

Retrieve an application&#39;s details and available pricing.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ApplicationBoardingApi();

var appId = 789; // Number | Application ID


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getApp(appId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **Number**| Application ID | 

### Return type

[**GetAppResult**](GetAppResult.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="saveData"></a>
# **saveData**
> QPApiResponse saveData(appId, opts)

Save Data

Saves one or more data fields to a new merchant application.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ApplicationBoardingApi();

var appId = 789; // Number | Application ID

var opts = { 
  'body': new QualpayPlatformApi.SaveDataRequest() // SaveDataRequest | Application Request
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.saveData(appId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **Number**| Application ID | 
 **body** | [**SaveDataRequest**](SaveDataRequest.md)| Application Request | [optional] 

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="uploadFile"></a>
# **uploadFile**
> QPApiResponse uploadFile(appId, file, bucket, opts)

Upload Document

Uploads a document relevant to this application to the provided bucket.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ApplicationBoardingApi();

var appId = 789; // Number | Application ID

var file = "/path/to/file.txt"; // File | The file to upload.

var bucket = "bucket_example"; // String | The bucket to upload the file into.

var opts = { 
  'label': "label_example" // String | An optional label to apply to the file.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.uploadFile(appId, file, bucket, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **Number**| Application ID | 
 **file** | **File**| The file to upload. | 
 **bucket** | **String**| The bucket to upload the file into. | 
 **label** | **String**| An optional label to apply to the file. | [optional] 

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="validateApp"></a>
# **validateApp**
> QPApiResponse validateApp(appId)

Validate Application

All data fields in an application are validated, including dependencies they may have between eachother. An application can not be submitted until it passes validation.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ApplicationBoardingApi();

var appId = 789; // Number | Application ID


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.validateApp(appId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **Number**| Application ID | 

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

