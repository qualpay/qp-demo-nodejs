# QualpayPlatformApi.QualpayCheckoutApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addCheckout**](QualpayCheckoutApi.md#addCheckout) | **POST** /checkout | Create a Checkout Payment Link
[**getDetails**](QualpayCheckoutApi.md#getDetails) | **GET** /checkout/{checkoutId} | Lookup Checkout Payment


<a name="addCheckout"></a>
# **addCheckout**
> CheckoutLinkResponse addCheckout(body)

Create a Checkout Payment Link

Creates a new checkout object which contains a payment link, through which checkout payments can be made. This operation generates a unique checkout_id, that can be used to query the status of a checkout payment using the Lookup Checkout Payment API.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.QualpayCheckoutApi();

var body = new QualpayPlatformApi.CheckoutRequest(); // CheckoutRequest | Checkout Resource


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.addCheckout(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CheckoutRequest**](CheckoutRequest.md)| Checkout Resource | 

### Return type

[**CheckoutLinkResponse**](CheckoutLinkResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getDetails"></a>
# **getDetails**
> CheckoutResponse getDetails(checkoutId)

Lookup Checkout Payment

Queries the status of a payment made through Qualpay Hosted Checkout. The operation returns a checkout object which contains information about the transaction.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.QualpayCheckoutApi();

var checkoutId = "checkoutId_example"; // String | Checkout ID


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getDetails(checkoutId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **checkoutId** | **String**| Checkout ID | 

### Return type

[**CheckoutResponse**](CheckoutResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

