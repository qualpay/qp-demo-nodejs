# QualpayPlatformApi.Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | [**HttpEntity**](HttpEntity.md) |  | 
**flash** | **{String: String}** |  | 
**session** | **{String: String}** |  | 
**cookies** | [**Cookies**](Cookies.md) |  | 


