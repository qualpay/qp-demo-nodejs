# QualpayPlatformApi.SendInvoiceRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**emailAddress** | **[String]** | An array of email addresses to which the invoice will be sent. By default, the system will send the invoice to the customer&#39;s email address (business_contact.email_address).  | [optional] 


