# QualpayPlatformApi.SaveDataRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**[ApplicationData]**](ApplicationData.md) | An array of data fields to save to the application. | 


