# QualpayPlatformApi.AusRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**[AusRequestData]**](AusRequestData.md) | An array of data fields to save to the Aus Requests. | 
**cardId** | **String** |  | 
**cardNumber** | **String** |  | 
**expDate** | **String** |  | 


