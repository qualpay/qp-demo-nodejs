# QualpayPlatformApi.AddAppRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **Number** | The sales channel to start this application in. The channel ID is provided to you by Qualpay. | 
**salesRep** | **Number** | The sales rep and contact for this application in. This is the qualpay ID for the sales rep user. The service at sales/browse can assist in finding the user_id. | 
**terminationNode** | **Number** | The point at which to board the merchant in the Qualpay hierarchy. A default node is configured for you, however you may choose a different node by using this field. | [optional] 


