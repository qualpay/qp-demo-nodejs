# QualpayPlatformApi.PaymentProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profileId** | **String** | The id of the payment profile. | [optional] 
**currency** | **String** | The currency code of the payment profile. | [optional] 
**autoCloseTime** | **Number** | The auto close time for the batch in Pacific Time. | [optional] 


