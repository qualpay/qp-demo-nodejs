# QualpayPlatformApi.LineItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quantity** | **Number** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 3 N&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;The count of items. | 
**description** | **String** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length AN&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;Description of the item. | 
**unitCost** | **Number** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 8,2 AN&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;Cost per unit, up to 2 decimal places. | 


