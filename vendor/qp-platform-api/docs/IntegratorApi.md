# QualpayPlatformApi.IntegratorApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createTestAccount**](IntegratorApi.md#createTestAccount) | **POST** /vendor/settings/account/add | Generate Test Account
[**getMerchantSettings**](IntegratorApi.md#getMerchantSettings) | **GET** /vendor/settings/{mid} | Merchant Settings
[**rotateApiKey**](IntegratorApi.md#rotateApiKey) | **POST** /vendor/settings/key/{mid} | Generate Security Key


<a name="createTestAccount"></a>
# **createTestAccount**
> MerchantSettingsResponse createTestAccount(opts)

Generate Test Account

Generates a new test merchant account.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.IntegratorApi();

var opts = { 
  'body': new QualpayPlatformApi.NewAccountRequest() // NewAccountRequest | NewAccount
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.createTestAccount(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**NewAccountRequest**](NewAccountRequest.md)| NewAccount | [optional] 

### Return type

[**MerchantSettingsResponse**](MerchantSettingsResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMerchantSettings"></a>
# **getMerchantSettings**
> MerchantSettingsResponse getMerchantSettings(mid)

Merchant Settings

Request the card types and currencies supported by the requested merchant ID.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.IntegratorApi();

var mid = 789; // Number | Merchant ID


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getMerchantSettings(mid, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mid** | **Number**| Merchant ID | 

### Return type

[**MerchantSettingsResponse**](MerchantSettingsResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="rotateApiKey"></a>
# **rotateApiKey**
> RotateKeyResponse rotateApiKey(mid, body)

Generate Security Key

Generates an API key for the specified merchant account.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.IntegratorApi();

var mid = 789; // Number | Merchant ID

var body = new QualpayPlatformApi.RotateKeyRequest(); // RotateKeyRequest | Data


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.rotateApiKey(mid, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mid** | **Number**| Merchant ID | 
 **body** | [**RotateKeyRequest**](RotateKeyRequest.md)| Data | 

### Return type

[**RotateKeyResponse**](RotateKeyResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

