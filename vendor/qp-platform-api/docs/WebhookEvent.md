# QualpayPlatformApi.WebhookEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**events** | **[String]** | An array of events that will trigger the POST request. Refer to Webhooks documentation for a list of available events. | 


