# QualpayPlatformApi.GetBillingCardsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billingCards** | [**[BillingCard]**](BillingCard.md) | &lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;An array of billing cards.  | [optional] 


