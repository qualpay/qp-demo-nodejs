# QualpayPlatformApi.ReportingApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**browseAusRequests**](ReportingApi.md#browseAusRequests) | **GET** /reporting/aus/detail | Account Updater Detail Report
[**browseAusSummary**](ReportingApi.md#browseAusSummary) | **GET** /reporting/aus/summary | Account Updater Summary Report
[**browseBatches**](ReportingApi.md#browseBatches) | **GET** /reporting/batches | Batch Report
[**browseDeposits**](ReportingApi.md#browseDeposits) | **GET** /reporting/deposits | Deposit Report
[**browseDisputes**](ReportingApi.md#browseDisputes) | **GET** /reporting/disputes | Disputes Report
[**browseTrans**](ReportingApi.md#browseTrans) | **GET** /reporting/transactions | Settled Transaction Report
[**getTransactionByPgId**](ReportingApi.md#getTransactionByPgId) | **GET** /reporting/transactions/bypgid/{pg_id} | Get transaction by PG ID
[**getTransactionRequests**](ReportingApi.md#getTransactionRequests) | **GET** /reporting/transaction-requests | Transaction Report


<a name="browseAusRequests"></a>
# **browseAusRequests**
> AccountUpdaterResponse browseAusRequests(opts)

Account Updater Detail Report

Browses a paginated list of Account updater requests

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ReportingApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "orderOn_example", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "orderBy_example", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseAusRequests(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] 
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] 
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**AccountUpdaterResponse**](AccountUpdaterResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="browseAusSummary"></a>
# **browseAusSummary**
> AccountUpdaterSummaryResponse browseAusSummary(opts)

Account Updater Summary Report

Browses a paginated list of account updater summary report

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ReportingApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "orderOn_example", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "orderBy_example", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseAusSummary(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] 
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] 
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**AccountUpdaterSummaryResponse**](AccountUpdaterSummaryResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="browseBatches"></a>
# **browseBatches**
> BatchResponse browseBatches(opts)

Batch Report

Browses a paginated list of merchant batches. These are whole batches which have been settled from the merchant&#39;s POS device, software, or gateway.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ReportingApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "orderOn_example", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "orderBy_example", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseBatches(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] 
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] 
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**BatchResponse**](BatchResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="browseDeposits"></a>
# **browseDeposits**
> DepositResponse browseDeposits(opts)

Deposit Report

Browses a paginated list of bank deposits and withdrawls.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ReportingApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "orderOn_example", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "orderBy_example", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseDeposits(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] 
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] 
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**DepositResponse**](DepositResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="browseDisputes"></a>
# **browseDisputes**
> DisputeResponse browseDisputes(opts)

Disputes Report

Browses a paginated list of disputes.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ReportingApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "orderOn_example", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "orderBy_example", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseDisputes(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] 
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] 
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**DisputeResponse**](DisputeResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="browseTrans"></a>
# **browseTrans**
> SettledTransactionResponse browseTrans(opts)

Settled Transaction Report

Browses a paginated list of settled transactions. Some additional text about deposits and stuff.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ReportingApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "orderOn_example", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "orderBy_example", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseTrans(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] 
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] 
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**SettledTransactionResponse**](SettledTransactionResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getTransactionByPgId"></a>
# **getTransactionByPgId**
> PGTransactionResponse getTransactionByPgId(pgId)

Get transaction by PG ID

Gets a payment gateway transaction by Payment Gateway ID.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ReportingApi();

var pgId = "pgId_example"; // String | PG ID


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getTransactionByPgId(pgId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pgId** | **String**| PG ID | 

### Return type

[**PGTransactionResponse**](PGTransactionResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getTransactionRequests"></a>
# **getTransactionRequests**
> TransactionRequestResponse getTransactionRequests(opts)

Transaction Report

Browses a paginated list of transaction requests

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.ReportingApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "tran_time", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "desc", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getTransactionRequests(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to tran_time]
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**TransactionRequestResponse**](TransactionRequestResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

