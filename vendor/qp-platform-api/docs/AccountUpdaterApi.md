# QualpayPlatformApi.AccountUpdaterApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addAusFileRequest**](AccountUpdaterApi.md#addAusFileRequest) | **POST** /aus/add/file | Submit AUS CSV File Request
[**addAusJsonRequest**](AccountUpdaterApi.md#addAusJsonRequest) | **POST** /aus/add | Submit AUS Request
[**getAusResponse**](AccountUpdaterApi.md#getAusResponse) | **GET** /aus/{requestId} | Get AUS Response


<a name="addAusFileRequest"></a>
# **addAusFileRequest**
> AusRequestResponse addAusFileRequest(file)

Submit AUS CSV File Request

Submit an Account Updater request using full card number and expiration date using csv file

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.AccountUpdaterApi();

var file = "/path/to/file.txt"; // File | The file to upload.


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.addAusFileRequest(file, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **File**| The file to upload. | 

### Return type

[**AusRequestResponse**](AusRequestResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="addAusJsonRequest"></a>
# **addAusJsonRequest**
> AusRequestResponse addAusJsonRequest(body)

Submit AUS Request

Submit an Account Updater request using full card number and expiration date.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.AccountUpdaterApi();

var body = new QualpayPlatformApi.AusRequest(); // AusRequest | aus request


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.addAusJsonRequest(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AusRequest**](AusRequest.md)| aus request | 

### Return type

[**AusRequestResponse**](AusRequestResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAusResponse"></a>
# **getAusResponse**
> AusUpdatedResponse getAusResponse(requestId)

Get AUS Response

Get updated AUS response using requestId

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.AccountUpdaterApi();

var requestId = 789; // Number | Request ID


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getAusResponse(requestId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | **Number**| Request ID | 

### Return type

[**AusUpdatedResponse**](AusUpdatedResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

