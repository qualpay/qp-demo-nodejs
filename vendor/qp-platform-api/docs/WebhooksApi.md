# QualpayPlatformApi.WebhooksApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addEvent**](WebhooksApi.md#addEvent) | **POST** /webhook/{webhook_id}/event/{event} | Add an event
[**addWebhook**](WebhooksApi.md#addWebhook) | **POST** /webhook | Add webhook
[**browseWebhook**](WebhooksApi.md#browseWebhook) | **GET** /webhook | Browse webhooks
[**disableWebhook**](WebhooksApi.md#disableWebhook) | **PUT** /webhook/{webhook_id}/disable | Disable a Webhook
[**editWebhook**](WebhooksApi.md#editWebhook) | **PUT** /webhook/{webhook_id} | Update webhook
[**enableWebhook**](WebhooksApi.md#enableWebhook) | **PUT** /webhook/{webhook_id}/enable | Enable a Webhook
[**getEvents**](WebhooksApi.md#getEvents) | **GET** /webhook/{webhook_id}/event | Get events
[**getWebhook**](WebhooksApi.md#getWebhook) | **GET** /webhook/{webhook_id} | Get webhook
[**removeEvent**](WebhooksApi.md#removeEvent) | **DELETE** /webhook/{webhook_id}/event/{event} | Delete event


<a name="addEvent"></a>
# **addEvent**
> QPApiResponse addEvent(webhookId, event, opts)

Add an event

Add an event to a webhook. Refer to the Webhook documentation for a list of available events. 

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.WebhooksApi();

var webhookId = 789; // Number | Webhook ID

var event = "event_example"; // String | Event

var opts = { 
  'body': new QualpayPlatformApi.QPApiData() // QPApiData | Webhook
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.addEvent(webhookId, event, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webhookId** | **Number**| Webhook ID | 
 **event** | **String**| Event | 
 **body** | [**QPApiData**](QPApiData.md)| Webhook | [optional] 

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="addWebhook"></a>
# **addWebhook**
> WebhookResponse addWebhook(opts)

Add webhook

Configure a new webhook. Save the webhook_id in the response for use in future requests. Save the generated secret, the secret can be used to validate the webhook. Refer to webhook documentation for secret usage.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.WebhooksApi();

var opts = { 
  'body': new QualpayPlatformApi.Webhook() // Webhook | Webhook
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.addWebhook(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Webhook**](Webhook.md)| Webhook | [optional] 

### Return type

[**WebhookResponse**](WebhookResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="browseWebhook"></a>
# **browseWebhook**
> WebhooksListResponse browseWebhook(opts)

Browse webhooks

Gets an array of webhook objects. Optional query parameters determines, size and sort order of returned array. Filters can be used to filter results. 

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.WebhooksApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "webhook_id", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "asc", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseWebhook(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to webhook_id]
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to asc]
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**WebhooksListResponse**](WebhooksListResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="disableWebhook"></a>
# **disableWebhook**
> QPApiResponse disableWebhook(webhookId, opts)

Disable a Webhook

Disable a webhook. Events will not be triggered on a disabled webhook. When disabled, all active requests for this webhook will be held. If the webhook is enabled before a request expires, Qualpay will attempt to post the request again.  

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.WebhooksApi();

var webhookId = 789; // Number | Webhook ID

var opts = { 
  'body': new QualpayPlatformApi.QPApiData() // QPApiData | Webhook
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.disableWebhook(webhookId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webhookId** | **Number**| Webhook ID | 
 **body** | [**QPApiData**](QPApiData.md)| Webhook | [optional] 

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="editWebhook"></a>
# **editWebhook**
> QPApiResponse editWebhook(webhookId, opts)

Update webhook

Manage a webhook. Once created, the webhook node cannot be modified. Only the fields to be updated can be sent in the request.  When events array is included in the request, all events will be replaced with the events in the request. 

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.WebhooksApi();

var webhookId = 789; // Number | Webhook ID

var opts = { 
  'body': new QualpayPlatformApi.Webhook() // Webhook | Webhook
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.editWebhook(webhookId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webhookId** | **Number**| Webhook ID | 
 **body** | [**Webhook**](Webhook.md)| Webhook | [optional] 

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="enableWebhook"></a>
# **enableWebhook**
> QPApiResponse enableWebhook(webhookId, opts)

Enable a Webhook

Enable a webhook. Events are triggered and requests are posted only for active webhooks. 

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.WebhooksApi();

var webhookId = 789; // Number | Webhook ID

var opts = { 
  'body': new QualpayPlatformApi.QPApiData() // QPApiData | Webhook
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.enableWebhook(webhookId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webhookId** | **Number**| Webhook ID | 
 **body** | [**QPApiData**](QPApiData.md)| Webhook | [optional] 

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getEvents"></a>
# **getEvents**
> WebhookEventResponse getEvents(webhookId)

Get events

Get all events for a webhook. Refer to the Webhook documentation for a list of available events. 

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.WebhooksApi();

var webhookId = 789; // Number | Webhook ID


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getEvents(webhookId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webhookId** | **Number**| Webhook ID | 

### Return type

[**WebhookEventResponse**](WebhookEventResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getWebhook"></a>
# **getWebhook**
> WebhookResponse getWebhook(webhookId)

Get webhook

Gets a webhook.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.WebhooksApi();

var webhookId = 789; // Number | Webhook ID


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getWebhook(webhookId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webhookId** | **Number**| Webhook ID | 

### Return type

[**WebhookResponse**](WebhookResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeEvent"></a>
# **removeEvent**
> QPApiResponse removeEvent(webhookId, event)

Delete event

Delete an event from a webhook. Refer to the Webhook documentation for a list of available events.  

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.WebhooksApi();

var webhookId = 789; // Number | Webhook ID

var event = "event_example"; // String | Event


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.removeEvent(webhookId, event, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webhookId** | **Number**| Webhook ID | 
 **event** | **String**| Event | 

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

