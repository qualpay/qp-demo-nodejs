# QualpayPlatformApi.AusRequestData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cardId** | **String** | Alphanumeric card id and can be up to 32 character. | 
**expDate** | **String** | Card expiration date in MMYY format. | 
**cardNumber** | **String** | Full card number. | 


