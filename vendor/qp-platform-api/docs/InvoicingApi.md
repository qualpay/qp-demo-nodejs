# QualpayPlatformApi.InvoicingApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addInvoicePayment**](InvoicingApi.md#addInvoicePayment) | **POST** /invoice/{invoice_id}/payments | Add Payment to an Invoice
[**browseBouncedInvoices**](InvoicingApi.md#browseBouncedInvoices) | **GET** /invoice/bounced | Get Undelivered Invoices
[**browseInvoicePayments**](InvoicingApi.md#browseInvoicePayments) | **GET** /invoice/payments | Get Invoice Payments
[**browseInvoicePaymentsById**](InvoicingApi.md#browseInvoicePaymentsById) | **GET** /invoice/{invoice_id}/payments | Get invoice payments by id
[**browseInvoices**](InvoicingApi.md#browseInvoices) | **GET** /invoice | Get all Invoices
[**cancelInvoice**](InvoicingApi.md#cancelInvoice) | **DELETE** /invoice/{invoice_id}/cancel | Cancel an Invoice
[**copyInvoice**](InvoicingApi.md#copyInvoice) | **POST** /invoice/{invoice_id}/copy | Copy an Invoice
[**createInvoice**](InvoicingApi.md#createInvoice) | **POST** /invoice | Create an invoice
[**getInvoice**](InvoicingApi.md#getInvoice) | **GET** /invoice/{invoice_id}/detail | Get by Invoice ID
[**removeInvoicePayment**](InvoicingApi.md#removeInvoicePayment) | **DELETE** /invoice/{invoice_id}/payments/{payment_id} | Remove an Invoice Payment
[**resendInvoice**](InvoicingApi.md#resendInvoice) | **POST** /invoice/{invoice_id}/resend | Resend an Invoice
[**sendInvoice**](InvoicingApi.md#sendInvoice) | **POST** /invoice/{invoice_id}/send | Send an Invoice
[**updateDraftInvoice**](InvoicingApi.md#updateDraftInvoice) | **PUT** /invoice/{invoice_id}/draft | Update a Draft Invoice
[**updateInvoicePayment**](InvoicingApi.md#updateInvoicePayment) | **PUT** /invoice/{invoice_id}/payments/{payment_id} | Update an Invoice Payment
[**updateOutstandingInvoice**](InvoicingApi.md#updateOutstandingInvoice) | **PUT** /invoice/{invoice_id}/outstanding | Update an Outstanding Invoice


<a name="addInvoicePayment"></a>
# **addInvoicePayment**
> InvoicePaymentResponse addInvoicePayment(invoiceId, body)

Add Payment to an Invoice

Adds a payment to an invoice. A check or cash payment can be added to a saved or outstanding invoice. Credit card payments cannot be added manually to an invoice. 

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var invoiceId = 789; // Number | 

var body = new QualpayPlatformApi.InvoicePaymentRequest(); // InvoicePaymentRequest | Invoice Payment


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.addInvoicePayment(invoiceId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceId** | **Number**|  | 
 **body** | [**InvoicePaymentRequest**](InvoicePaymentRequest.md)| Invoice Payment | 

### Return type

[**InvoicePaymentResponse**](InvoicePaymentResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="browseBouncedInvoices"></a>
# **browseBouncedInvoices**
> InvoiceBouncedResponse browseBouncedInvoices(opts)

Get Undelivered Invoices

Browse all undelivered invoices. Optional query parameters determines, size and sort order of returned array.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "db_timestamp", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "desc", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseBouncedInvoices(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to db_timestamp]
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**InvoiceBouncedResponse**](InvoiceBouncedResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="browseInvoicePayments"></a>
# **browseInvoicePayments**
> InvoicePaymentListResponse browseInvoicePayments(opts)

Get Invoice Payments

Browse all invoice payments. Optional query parameters determines, size and sort order of returned array.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "date_payment", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "desc", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseInvoicePayments(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to date_payment]
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**InvoicePaymentListResponse**](InvoicePaymentListResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="browseInvoicePaymentsById"></a>
# **browseInvoicePaymentsById**
> InvoicePaymentListResponse browseInvoicePaymentsById(invoiceId, opts)

Get invoice payments by id

Browse all invoice payments made to an invoice. Optional query parameters determines, size and sort order of returned array.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var invoiceId = 789; // Number | 

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "date_payment", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "desc", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseInvoicePaymentsById(invoiceId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceId** | **Number**|  | 
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to date_payment]
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**InvoicePaymentListResponse**](InvoicePaymentListResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="browseInvoices"></a>
# **browseInvoices**
> InvoiceListResponse browseInvoices(opts)

Get all Invoices

Gets an array of invoice objects. Optional query parameters determines, size and sort order of returned array.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "invoice_number", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "desc", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseInvoices(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to invoice_number]
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**InvoiceListResponse**](InvoiceListResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="cancelInvoice"></a>
# **cancelInvoice**
> InvoiceResponse cancelInvoice(invoiceId)

Cancel an Invoice

Cancels an invoice. A canceled invoice cannot be edited. If your customer clicks on the pay now button in the invoice e-mail an error message will be displayed.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var invoiceId = 789; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.cancelInvoice(invoiceId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceId** | **Number**|  | 

### Return type

[**InvoiceResponse**](InvoiceResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="copyInvoice"></a>
# **copyInvoice**
> InvoiceResponse copyInvoice(invoiceId, opts)

Copy an Invoice

Makes a copy of an invoice. The invoice date will be set to today&#39;s date and due date will be adjusted based on the invoice date and the payment terms. Optionally, include an invoice_number in the POST body to make a copy of the invoice with a different invoice number. Invoice payments from the original invoice will not be copied. 

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var invoiceId = 789; // Number | 

var opts = { 
  'body': new QualpayPlatformApi.CopyInvoiceRequest() // CopyInvoiceRequest | Invoice
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.copyInvoice(invoiceId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceId** | **Number**|  | 
 **body** | [**CopyInvoiceRequest**](CopyInvoiceRequest.md)| Invoice | [optional] 

### Return type

[**InvoiceResponse**](InvoiceResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createInvoice"></a>
# **createInvoice**
> InvoiceResponse createInvoice(body)

Create an invoice

Creates a draft invoice that you can send later using the Send Invoice API.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var body = new QualpayPlatformApi.CreateInvoiceRequest(); // CreateInvoiceRequest | Invoice


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.createInvoice(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateInvoiceRequest**](CreateInvoiceRequest.md)| Invoice | 

### Return type

[**InvoiceResponse**](InvoiceResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getInvoice"></a>
# **getInvoice**
> InvoiceResponse getInvoice(invoiceId)

Get by Invoice ID

Gets an invoice by invoice_id.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var invoiceId = 789; // Number | Invoice ID


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getInvoice(invoiceId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceId** | **Number**| Invoice ID | 

### Return type

[**InvoiceResponse**](InvoiceResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeInvoicePayment"></a>
# **removeInvoicePayment**
> QPApiResponse removeInvoicePayment(invoiceId, paymentId)

Remove an Invoice Payment

Removes an invoice payment. A payment can be removed on a saved or an outstanding invoice. CARD type payments cannot be removed. Payments made via a credit card cannot be removed. Payments can be deleted only from SAVED or OUTSTANDING invoices.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var invoiceId = 789; // Number | 

var paymentId = 789; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.removeInvoicePayment(invoiceId, paymentId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceId** | **Number**|  | 
 **paymentId** | **Number**|  | 

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="resendInvoice"></a>
# **resendInvoice**
> QPApiResponse resendInvoice(invoiceId, opts)

Resend an Invoice

Resends an invoice to the customer. An outstanding or paid invoice can be resent.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var invoiceId = 789; // Number | 

var opts = { 
  'body': new QualpayPlatformApi.ResendInvoiceRequest() // ResendInvoiceRequest | Email Addresses
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.resendInvoice(invoiceId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceId** | **Number**|  | 
 **body** | [**ResendInvoiceRequest**](ResendInvoiceRequest.md)| Email Addresses | [optional] 

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="sendInvoice"></a>
# **sendInvoice**
> InvoiceResponse sendInvoice(invoiceId, opts)

Send an Invoice

Sends an invoice to the customer. Sending an invoice changes the status of the invoice to outstanding. Once sent, only the from_contact and business_contact of the invoice can be updated.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var invoiceId = 789; // Number | 

var opts = { 
  'body': new QualpayPlatformApi.SendInvoiceRequest() // SendInvoiceRequest | Email Addresses
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.sendInvoice(invoiceId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceId** | **Number**|  | 
 **body** | [**SendInvoiceRequest**](SendInvoiceRequest.md)| Email Addresses | [optional] 

### Return type

[**InvoiceResponse**](InvoiceResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateDraftInvoice"></a>
# **updateDraftInvoice**
> InvoiceResponse updateDraftInvoice(invoiceId, body)

Update a Draft Invoice

Updates a draft invoice. Only the fields that need updating can be sent in the request body. If updating JSON object fields, the complete JSON should be sent in the request body. 

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var invoiceId = 789; // Number | 

var body = new QualpayPlatformApi.UpdateDraftRequest(); // UpdateDraftRequest | Invoice


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateDraftInvoice(invoiceId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceId** | **Number**|  | 
 **body** | [**UpdateDraftRequest**](UpdateDraftRequest.md)| Invoice | 

### Return type

[**InvoiceResponse**](InvoiceResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateInvoicePayment"></a>
# **updateInvoicePayment**
> InvoicePaymentResponse updateInvoicePayment(invoiceId, paymentId, body)

Update an Invoice Payment

Updates an invoice payment. A payment can be updated on a saved or an outstanding invoice. Payments made via credit card using the “Pay Now” button cannot be updated.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var invoiceId = 789; // Number | 

var paymentId = 789; // Number | 

var body = new QualpayPlatformApi.InvoicePaymentRequest(); // InvoicePaymentRequest | Invoice Payment


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateInvoicePayment(invoiceId, paymentId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceId** | **Number**|  | 
 **paymentId** | **Number**|  | 
 **body** | [**InvoicePaymentRequest**](InvoicePaymentRequest.md)| Invoice Payment | 

### Return type

[**InvoicePaymentResponse**](InvoicePaymentResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateOutstandingInvoice"></a>
# **updateOutstandingInvoice**
> InvoiceResponse updateOutstandingInvoice(invoiceId, body)

Update an Outstanding Invoice

Updates an outstanding invoice. Only the from_contact and business_contact fields can be updated on an outstanding invoice. Only the fields that need updating can be sent in the request body. If updating JSON object fields, the complete JSON should be sent in the request body. 

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.InvoicingApi();

var invoiceId = 789; // Number | 

var body = new QualpayPlatformApi.UpdateOutstandingRequest(); // UpdateOutstandingRequest | Invoice


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateOutstandingInvoice(invoiceId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceId** | **Number**|  | 
 **body** | [**UpdateOutstandingRequest**](UpdateOutstandingRequest.md)| Invoice | 

### Return type

[**InvoiceResponse**](InvoiceResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

