# QualpayPlatformApi.PricingModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pricingId** | **Number** | Unique ID assigned by Qualpay to this pricing plan. | [optional] 
**plan** | [**PlanModel**](PlanModel.md) | The pricing plan object. | [optional] 


