# QualpayPlatformApi.Payment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cardType** | **String** | The two character abbreviation for the card type. | [optional] 
**currency** | **[String]** | An array of currency codes allowed for the specified card type. | [optional] 


