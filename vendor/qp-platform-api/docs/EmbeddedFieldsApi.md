# QualpayPlatformApi.EmbeddedFieldsApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getEmbeddedTransientKey**](EmbeddedFieldsApi.md#getEmbeddedTransientKey) | **GET** /embedded | Get Transient Key


<a name="getEmbeddedTransientKey"></a>
# **getEmbeddedTransientKey**
> EmbeddedKeyResponse getEmbeddedTransientKey()

Get Transient Key

Gets a transient key for use with Qualpay Embedded Fields. This key will be invalidated in 12 hours or when a card is successfully verified using Embedded Fields. The transient key is a one time key that is used to invoke Embedded Fields.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.EmbeddedFieldsApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getEmbeddedTransientKey(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**EmbeddedKeyResponse**](EmbeddedKeyResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

