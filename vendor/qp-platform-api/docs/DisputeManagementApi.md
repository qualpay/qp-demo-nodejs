# QualpayPlatformApi.DisputeManagementApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**acceptDispute**](DisputeManagementApi.md#acceptDispute) | **GET** /dispute/{recId}/accept | Accept Dispute
[**browseDisputes**](DisputeManagementApi.md#browseDisputes) | **GET** /dispute/browse | Get Disputes
[**createDispute**](DisputeManagementApi.md#createDispute) | **GET** /dispute/{merchantId}/create | Create Dispute Data
[**getCreditedTran**](DisputeManagementApi.md#getCreditedTran) | **GET** /dispute/{recId}/creditedtrans | Get Credited Transaction Detail
[**getDisputeResponse**](DisputeManagementApi.md#getDisputeResponse) | **GET** /dispute/{recId}/response | Get Submitted Dispute Response
[**getNonDisputedTran**](DisputeManagementApi.md#getNonDisputedTran) | **GET** /dispute/{recId}/nondisputedtrans | Get Non Disputed Transaction 
[**resetDispute**](DisputeManagementApi.md#resetDispute) | **GET** /dispute/{recId}/reset | Reset Dispute Data
[**submitDisputesResponse**](DisputeManagementApi.md#submitDisputesResponse) | **POST** /dispute/{recId}/respond | Submit Dispute Response


<a name="acceptDispute"></a>
# **acceptDispute**
> QPApiResponse acceptDispute(recId)

Accept Dispute

Accept first time Chargeback and Pre-Arbitration Withdraw from Arbitration.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.DisputeManagementApi();

var recId = 789; // Number | Control Number


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.acceptDispute(recId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recId** | **Number**| Control Number | 

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="browseDisputes"></a>
# **browseDisputes**
> DisputeDetail browseDisputes(opts)

Get Disputes

Request all disputes and their detail associated with a vendor or a node.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.DisputeManagementApi();

var opts = { 
  'count': 10, // Number | The number of records in the result.
  'orderOn': "date_payment", // String | The field on which the results will be sorted on. Refer to the response model for available fields.
  'orderBy': "desc", // String | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
  'page': 0, // Number | Zero-based page number, use this to choose a page when there are more results than the count parameter.
  'filter': "filter_example" // String | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.browseDisputes(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Number**| The number of records in the result. | [optional] [default to 10]
 **orderOn** | **String**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to date_payment]
 **orderBy** | **String**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **Number**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **String**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional] 

### Return type

[**DisputeDetail**](DisputeDetail.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createDispute"></a>
# **createDispute**
> QPApiListResponse createDispute(merchantId, opts)

Create Dispute Data

For testing purposes, create a dispute for a specific reason code or for all the reason codes.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.DisputeManagementApi();

var merchantId = 789; // Number | Merchant ID

var opts = { 
  'reasonCode': "null" // String | Reason Code
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.createDispute(merchantId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchantId** | **Number**| Merchant ID | 
 **reasonCode** | **String**| Reason Code | [optional] [default to null]

### Return type

[**QPApiListResponse**](QPApiListResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCreditedTran"></a>
# **getCreditedTran**
> CorrespondingTransactionResponse getCreditedTran(recId)

Get Credited Transaction Detail

Request the credited transactions associated with dispute.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.DisputeManagementApi();

var recId = 789; // Number | Control Number


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getCreditedTran(recId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recId** | **Number**| Control Number | 

### Return type

[**CorrespondingTransactionResponse**](CorrespondingTransactionResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getDisputeResponse"></a>
# **getDisputeResponse**
> DisputeResponse getDisputeResponse(recId)

Get Submitted Dispute Response

Get previously submitted dispute response.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.DisputeManagementApi();

var recId = 789; // Number | Control Number


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getDisputeResponse(recId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recId** | **Number**| Control Number | 

### Return type

[**DisputeResponse**](DisputeResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getNonDisputedTran"></a>
# **getNonDisputedTran**
> CorrespondingTransactionResponse getNonDisputedTran(recId)

Get Non Disputed Transaction 

Request the non-disputed transactions associated with with dispute.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.DisputeManagementApi();

var recId = 789; // Number | Control Number


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getNonDisputedTran(recId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recId** | **Number**| Control Number | 

### Return type

[**CorrespondingTransactionResponse**](CorrespondingTransactionResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="resetDispute"></a>
# **resetDispute**
> QPApiResponse resetDispute(recId)

Reset Dispute Data

For non production enviornment, request to reset a dispute case to new status.For Production, request to reset dispute to new status till a dispute is in Qualpay Review (Status&#x3D;Q) 

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.DisputeManagementApi();

var recId = 789; // Number | Control Number


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.resetDispute(recId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recId** | **Number**| Control Number | 

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="submitDisputesResponse"></a>
# **submitDisputesResponse**
> QPApiResponse submitDisputesResponse(recId, file, response)

Submit Dispute Response

Submit dispute responses with supporting documentation. Response options are dynamic and are based on the reason code.

### Example
```javascript
var QualpayPlatformApi = require('qualpay_platform_api');
var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

var apiInstance = new QualpayPlatformApi.DisputeManagementApi();

var recId = 789; // Number | Control Number

var file = "/path/to/file.txt"; // File | The file to upload.

var response = "response_example"; // String | Dispute response


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.submitDisputesResponse(recId, file, response, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recId** | **Number**| Control Number | 
 **file** | **File**| The file to upload. | 
 **response** | **String**| Dispute response | 

### Return type

[**QPApiResponse**](QPApiResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

