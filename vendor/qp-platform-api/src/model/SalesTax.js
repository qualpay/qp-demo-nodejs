/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.QualpayPlatformApi) {
      root.QualpayPlatformApi = {};
    }
    root.QualpayPlatformApi.SalesTax = factory(root.QualpayPlatformApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The SalesTax model module.
   * @module model/SalesTax
   * @version 1.1.9
   */

  /**
   * Constructs a new <code>SalesTax</code>.
   * @alias module:model/SalesTax
   * @class
   */
  var exports = function() {
    var _this = this;



  };

  /**
   * Constructs a <code>SalesTax</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/SalesTax} obj Optional instance to populate.
   * @return {module:model/SalesTax} The populated <code>SalesTax</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('type')) {
        obj['type'] = ApiClient.convertToType(data['type'], 'String');
      }
      if (data.hasOwnProperty('value')) {
        obj['value'] = ApiClient.convertToType(data['value'], 'Number');
      }
    }
    return obj;
  }

  /**
   * <strong>Format: </strong>Variable length, up to 10 AN<br><strong>Description: </strong>Sales Tax type. Possible values are  <br><strong>RATE<strong> The value field contains the tax rate. The system will automatically calculate the tax amount based on the rate.  <br><strong>AMOUNT<strong> The value field contains the tax amount.
   * @member {module:model/SalesTax.TypeEnum} type
   */
  exports.prototype['type'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 7,5 N<br><strong>Description: </strong>Sales Tax value.If type is RATE, set this field to the sales tax rate. The system will automatically calculate the  sales tax amount (amt_tax) based on the rate. <br>If type is AMOUNT, set this field to the sales tax amount.
   * @member {Number} value
   */
  exports.prototype['value'] = undefined;


  /**
   * Allowed values for the <code>type</code> property.
   * @enum {String}
   * @readonly
   */
  exports.TypeEnum = {
    /**
     * value: "RATE"
     * @const
     */
    "RATE": "RATE",
    /**
     * value: "AMOUNT"
     * @const
     */
    "AMOUNT": "AMOUNT"  };


  return exports;
}));


