/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.QualpayPlatformApi) {
      root.QualpayPlatformApi = {};
    }
    root.QualpayPlatformApi.PaymentProfile = factory(root.QualpayPlatformApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The PaymentProfile model module.
   * @module model/PaymentProfile
   * @version 1.1.9
   */

  /**
   * Constructs a new <code>PaymentProfile</code>.
   * @alias module:model/PaymentProfile
   * @class
   */
  var exports = function() {
    var _this = this;




  };

  /**
   * Constructs a <code>PaymentProfile</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PaymentProfile} obj Optional instance to populate.
   * @return {module:model/PaymentProfile} The populated <code>PaymentProfile</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('profile_id')) {
        obj['profile_id'] = ApiClient.convertToType(data['profile_id'], 'String');
      }
      if (data.hasOwnProperty('currency')) {
        obj['currency'] = ApiClient.convertToType(data['currency'], 'String');
      }
      if (data.hasOwnProperty('auto_close_time')) {
        obj['auto_close_time'] = ApiClient.convertToType(data['auto_close_time'], 'Number');
      }
    }
    return obj;
  }

  /**
   * The id of the payment profile.
   * @member {String} profile_id
   */
  exports.prototype['profile_id'] = undefined;
  /**
   * The currency code of the payment profile.
   * @member {String} currency
   */
  exports.prototype['currency'] = undefined;
  /**
   * The auto close time for the batch in Pacific Time.
   * @member {Number} auto_close_time
   */
  exports.prototype['auto_close_time'] = undefined;



  return exports;
}));


