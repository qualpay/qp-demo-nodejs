/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.QualpayPlatformApi) {
      root.QualpayPlatformApi = {};
    }
    root.QualpayPlatformApi.UpdateSubscriptionRequest = factory(root.QualpayPlatformApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The UpdateSubscriptionRequest model module.
   * @module model/UpdateSubscriptionRequest
   * @version 1.1.9
   */

  /**
   * Constructs a new <code>UpdateSubscriptionRequest</code>.
   * @alias module:model/UpdateSubscriptionRequest
   * @class
   * @param dateStart {String} <strong>Format: </strong>Fixed length, 10, YYYY-MM-DD format<br><strong>Description: </strong>Date the subscription will start.
   */
  var exports = function(dateStart) {
    var _this = this;

    _this['date_start'] = dateStart;

  };

  /**
   * Constructs a <code>UpdateSubscriptionRequest</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/UpdateSubscriptionRequest} obj Optional instance to populate.
   * @return {module:model/UpdateSubscriptionRequest} The populated <code>UpdateSubscriptionRequest</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('date_start')) {
        obj['date_start'] = ApiClient.convertToType(data['date_start'], 'String');
      }
      if (data.hasOwnProperty('merchant_id')) {
        obj['merchant_id'] = ApiClient.convertToType(data['merchant_id'], 'Number');
      }
    }
    return obj;
  }

  /**
   * <strong>Format: </strong>Fixed length, 10, YYYY-MM-DD format<br><strong>Description: </strong>Date the subscription will start.
   * @member {String} date_start
   */
  exports.prototype['date_start'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 16 AN<br><strong>Description: </strong>Identifies the merchant to whom this request applies. Optional field, applicable only if the request is sent on behalf of another merchant.<br><strong>Conditional Requirement: </strong>Required if this request is on behalf of another merchant.
   * @member {Number} merchant_id
   */
  exports.prototype['merchant_id'] = undefined;



  return exports;
}));


