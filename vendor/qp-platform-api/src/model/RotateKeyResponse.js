/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ApiKey'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./ApiKey'));
  } else {
    // Browser globals (root is window)
    if (!root.QualpayPlatformApi) {
      root.QualpayPlatformApi = {};
    }
    root.QualpayPlatformApi.RotateKeyResponse = factory(root.QualpayPlatformApi.ApiClient, root.QualpayPlatformApi.ApiKey);
  }
}(this, function(ApiClient, ApiKey) {
  'use strict';




  /**
   * The RotateKeyResponse model module.
   * @module model/RotateKeyResponse
   * @version 1.1.9
   */

  /**
   * Constructs a new <code>RotateKeyResponse</code>.
   * @alias module:model/RotateKeyResponse
   * @class
   */
  var exports = function() {
    var _this = this;




  };

  /**
   * Constructs a <code>RotateKeyResponse</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/RotateKeyResponse} obj Optional instance to populate.
   * @return {module:model/RotateKeyResponse} The populated <code>RotateKeyResponse</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('code')) {
        obj['code'] = ApiClient.convertToType(data['code'], 'Number');
      }
      if (data.hasOwnProperty('message')) {
        obj['message'] = ApiClient.convertToType(data['message'], 'String');
      }
      if (data.hasOwnProperty('data')) {
        obj['data'] = ApiKey.constructFromObject(data['data']);
      }
    }
    return obj;
  }

  /**
   * <strong>Format: </strong>Fixed length, 1 N<br><strong>Description: </strong>Response code from API. 0 indicates success. Refer to <a href=\"/developer/api/reference#api-response-codes\"target=\"_blank\">Platform API Response Codes</a> for entire list of return codes.
   * @member {Number} code
   */
  exports.prototype['code'] = undefined;
  /**
   * <strong>Format: </strong>Variable length AN<br><strong>Description: </strong>A short description of the API response code.
   * @member {String} message
   */
  exports.prototype['message'] = undefined;
  /**
   * <br><strong>Description: </strong>If request is successful, this field will contain the response resource. If there are input validation errors i.e, the code is 2, this field may contain a list of fields that failed the validation.
   * @member {module:model/ApiKey} data
   */
  exports.prototype['data'] = undefined;



  return exports;
}));


