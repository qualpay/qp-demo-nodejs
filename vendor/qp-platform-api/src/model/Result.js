/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Cookies', 'model/HttpEntity'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Cookies'), require('./HttpEntity'));
  } else {
    // Browser globals (root is window)
    if (!root.QualpayPlatformApi) {
      root.QualpayPlatformApi = {};
    }
    root.QualpayPlatformApi.Result = factory(root.QualpayPlatformApi.ApiClient, root.QualpayPlatformApi.Cookies, root.QualpayPlatformApi.HttpEntity);
  }
}(this, function(ApiClient, Cookies, HttpEntity) {
  'use strict';




  /**
   * The Result model module.
   * @module model/Result
   * @version 1.1.9
   */

  /**
   * Constructs a new <code>Result</code>.
   * @alias module:model/Result
   * @class
   * @param body {module:model/HttpEntity} 
   * @param flash {Object.<String, String>} 
   * @param session {Object.<String, String>} 
   * @param cookies {module:model/Cookies} 
   */
  var exports = function(body, flash, session, cookies) {
    var _this = this;

    _this['body'] = body;
    _this['flash'] = flash;
    _this['session'] = session;
    _this['cookies'] = cookies;
  };

  /**
   * Constructs a <code>Result</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Result} obj Optional instance to populate.
   * @return {module:model/Result} The populated <code>Result</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('body')) {
        obj['body'] = HttpEntity.constructFromObject(data['body']);
      }
      if (data.hasOwnProperty('flash')) {
        obj['flash'] = ApiClient.convertToType(data['flash'], {'String': 'String'});
      }
      if (data.hasOwnProperty('session')) {
        obj['session'] = ApiClient.convertToType(data['session'], {'String': 'String'});
      }
      if (data.hasOwnProperty('cookies')) {
        obj['cookies'] = Cookies.constructFromObject(data['cookies']);
      }
    }
    return obj;
  }

  /**
   * @member {module:model/HttpEntity} body
   */
  exports.prototype['body'] = undefined;
  /**
   * @member {Object.<String, String>} flash
   */
  exports.prototype['flash'] = undefined;
  /**
   * @member {Object.<String, String>} session
   */
  exports.prototype['session'] = undefined;
  /**
   * @member {module:model/Cookies} cookies
   */
  exports.prototype['cookies'] = undefined;



  return exports;
}));


