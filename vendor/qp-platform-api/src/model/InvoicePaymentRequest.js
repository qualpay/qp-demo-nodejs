/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.QualpayPlatformApi) {
      root.QualpayPlatformApi = {};
    }
    root.QualpayPlatformApi.InvoicePaymentRequest = factory(root.QualpayPlatformApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The InvoicePaymentRequest model module.
   * @module model/InvoicePaymentRequest
   * @version 1.1.9
   */

  /**
   * Constructs a new <code>InvoicePaymentRequest</code>.
   * @alias module:model/InvoicePaymentRequest
   * @class
   * @param type {module:model/InvoicePaymentRequest.TypeEnum} <strong>Format: </strong>Variable length, up to 16 AN<br><strong>Description: </strong>The mode of payment. <br><ul><li><strong>CARD</strong> Invoice checkout payment by customer using a card. A card payment cannot be manually added. </li><li><strong>CASH</strong> Cash payment.</li><li><strong>CHECK</strong> Check Payment.</li><li><strong>OTHER</strong> Other modes of payment.</li></ul>
   * @param datePayment {String} <strong>Format: </strong>Variable length, up to 10 AN, in YYYY-MM-DD format<br><strong>Description: </strong>Date the payment was made.
   * @param amtPaid {Number} <strong>Format: </strong>Variable length, up to 10, 2 N<br><strong>Description: </strong>Amount paid. The payment currency should be the same as the invoice currency. 
   */
  var exports = function(type, datePayment, amtPaid) {
    var _this = this;

    _this['type'] = type;
    _this['date_payment'] = datePayment;
    _this['amt_paid'] = amtPaid;

  };

  /**
   * Constructs a <code>InvoicePaymentRequest</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/InvoicePaymentRequest} obj Optional instance to populate.
   * @return {module:model/InvoicePaymentRequest} The populated <code>InvoicePaymentRequest</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('type')) {
        obj['type'] = ApiClient.convertToType(data['type'], 'String');
      }
      if (data.hasOwnProperty('date_payment')) {
        obj['date_payment'] = ApiClient.convertToType(data['date_payment'], 'String');
      }
      if (data.hasOwnProperty('amt_paid')) {
        obj['amt_paid'] = ApiClient.convertToType(data['amt_paid'], 'Number');
      }
      if (data.hasOwnProperty('description')) {
        obj['description'] = ApiClient.convertToType(data['description'], 'String');
      }
    }
    return obj;
  }

  /**
   * <strong>Format: </strong>Variable length, up to 16 AN<br><strong>Description: </strong>The mode of payment. <br><ul><li><strong>CARD</strong> Invoice checkout payment by customer using a card. A card payment cannot be manually added. </li><li><strong>CASH</strong> Cash payment.</li><li><strong>CHECK</strong> Check Payment.</li><li><strong>OTHER</strong> Other modes of payment.</li></ul>
   * @member {module:model/InvoicePaymentRequest.TypeEnum} type
   */
  exports.prototype['type'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 10 AN, in YYYY-MM-DD format<br><strong>Description: </strong>Date the payment was made.
   * @member {String} date_payment
   */
  exports.prototype['date_payment'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 10, 2 N<br><strong>Description: </strong>Amount paid. The payment currency should be the same as the invoice currency. 
   * @member {Number} amt_paid
   */
  exports.prototype['amt_paid'] = undefined;
  /**
   * <strong>Format: </strong>Variable length<br><strong>Description: </strong>A short description of the payment.
   * @member {String} description
   */
  exports.prototype['description'] = undefined;


  /**
   * Allowed values for the <code>type</code> property.
   * @enum {String}
   * @readonly
   */
  exports.TypeEnum = {
    /**
     * value: "CASH"
     * @const
     */
    "CASH": "CASH",
    /**
     * value: "CHECK"
     * @const
     */
    "CHECK": "CHECK",
    /**
     * value: "OTHER"
     * @const
     */
    "OTHER": "OTHER"  };


  return exports;
}));


