/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.QualpayPlatformApi) {
      root.QualpayPlatformApi = {};
    }
    root.QualpayPlatformApi.DepositReport = factory(root.QualpayPlatformApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The DepositReport model module.
   * @module model/DepositReport
   * @version 1.1.9
   */

  /**
   * Constructs a new <code>DepositReport</code>.
   * @alias module:model/DepositReport
   * @class
   * @param recId {Number} <strong>Format: </strong>Variable length, up to 18 N<br><strong>Description: </strong>Unique ID assigned by Qualpay to this deposit.
   * @param merchantId {Number} <strong>Format: </strong>Variable length, up to 16 N<br><strong>Description: </strong>Unique ID assigned by Qualpay to a merchant.
   * @param dbaName {String} <strong>Format: </strong>Variable length, up to 25 AN<br><strong>Description: </strong>The doing business as name of the merchant.
   * @param referenceNumber {String} <strong>Format: </strong>Variable length, up to 16 N<br><strong>Description: </strong>The bank reference number of the deposit.
   * @param amtAch {String} <strong>Format: </strong>Variable length, up to 17,2 N<br><strong>Description: </strong>ACH amount.
   * @param achDescription {String} <strong>Format: </strong>Variable length, up to 18 AN<br><strong>Description: </strong>ACH description.
   * @param postDateActual {String} <strong>Format: </strong>Variable length, up to 10 AN, in YYYY-MM-DD format<br><strong>Description: </strong>The date the ACH posted to the federal reserve. 
   */
  var exports = function(recId, merchantId, dbaName, referenceNumber, amtAch, achDescription, postDateActual) {
    var _this = this;

    _this['rec_id'] = recId;
    _this['merchant_id'] = merchantId;
    _this['dba_name'] = dbaName;
    _this['reference_number'] = referenceNumber;
    _this['amt_ach'] = amtAch;
    _this['ach_description'] = achDescription;
    _this['post_date_actual'] = postDateActual;
  };

  /**
   * Constructs a <code>DepositReport</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DepositReport} obj Optional instance to populate.
   * @return {module:model/DepositReport} The populated <code>DepositReport</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('rec_id')) {
        obj['rec_id'] = ApiClient.convertToType(data['rec_id'], 'Number');
      }
      if (data.hasOwnProperty('merchant_id')) {
        obj['merchant_id'] = ApiClient.convertToType(data['merchant_id'], 'Number');
      }
      if (data.hasOwnProperty('dba_name')) {
        obj['dba_name'] = ApiClient.convertToType(data['dba_name'], 'String');
      }
      if (data.hasOwnProperty('reference_number')) {
        obj['reference_number'] = ApiClient.convertToType(data['reference_number'], 'String');
      }
      if (data.hasOwnProperty('amt_ach')) {
        obj['amt_ach'] = ApiClient.convertToType(data['amt_ach'], 'String');
      }
      if (data.hasOwnProperty('ach_description')) {
        obj['ach_description'] = ApiClient.convertToType(data['ach_description'], 'String');
      }
      if (data.hasOwnProperty('post_date_actual')) {
        obj['post_date_actual'] = ApiClient.convertToType(data['post_date_actual'], 'String');
      }
    }
    return obj;
  }

  /**
   * <strong>Format: </strong>Variable length, up to 18 N<br><strong>Description: </strong>Unique ID assigned by Qualpay to this deposit.
   * @member {Number} rec_id
   */
  exports.prototype['rec_id'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 16 N<br><strong>Description: </strong>Unique ID assigned by Qualpay to a merchant.
   * @member {Number} merchant_id
   */
  exports.prototype['merchant_id'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 25 AN<br><strong>Description: </strong>The doing business as name of the merchant.
   * @member {String} dba_name
   */
  exports.prototype['dba_name'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 16 N<br><strong>Description: </strong>The bank reference number of the deposit.
   * @member {String} reference_number
   */
  exports.prototype['reference_number'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 17,2 N<br><strong>Description: </strong>ACH amount.
   * @member {String} amt_ach
   */
  exports.prototype['amt_ach'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 18 AN<br><strong>Description: </strong>ACH description.
   * @member {String} ach_description
   */
  exports.prototype['ach_description'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 10 AN, in YYYY-MM-DD format<br><strong>Description: </strong>The date the ACH posted to the federal reserve. 
   * @member {String} post_date_actual
   */
  exports.prototype['post_date_actual'] = undefined;



  return exports;
}));


