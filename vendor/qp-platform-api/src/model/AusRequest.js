/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/AusRequestData'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./AusRequestData'));
  } else {
    // Browser globals (root is window)
    if (!root.QualpayPlatformApi) {
      root.QualpayPlatformApi = {};
    }
    root.QualpayPlatformApi.AusRequest = factory(root.QualpayPlatformApi.ApiClient, root.QualpayPlatformApi.AusRequestData);
  }
}(this, function(ApiClient, AusRequestData) {
  'use strict';




  /**
   * The AusRequest model module.
   * @module model/AusRequest
   * @version 1.1.9
   */

  /**
   * Constructs a new <code>AusRequest</code>.
   * @alias module:model/AusRequest
   * @class
   * @param data {Array.<module:model/AusRequestData>} An array of data fields to save to the Aus Requests.
   * @param cardId {String} 
   * @param cardNumber {String} 
   * @param expDate {String} 
   */
  var exports = function(data, cardId, cardNumber, expDate) {
    var _this = this;

    _this['data'] = data;
    _this['card_id'] = cardId;
    _this['card_number'] = cardNumber;
    _this['exp_date'] = expDate;
  };

  /**
   * Constructs a <code>AusRequest</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/AusRequest} obj Optional instance to populate.
   * @return {module:model/AusRequest} The populated <code>AusRequest</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('data')) {
        obj['data'] = ApiClient.convertToType(data['data'], [AusRequestData]);
      }
      if (data.hasOwnProperty('card_id')) {
        obj['card_id'] = ApiClient.convertToType(data['card_id'], 'String');
      }
      if (data.hasOwnProperty('card_number')) {
        obj['card_number'] = ApiClient.convertToType(data['card_number'], 'String');
      }
      if (data.hasOwnProperty('exp_date')) {
        obj['exp_date'] = ApiClient.convertToType(data['exp_date'], 'String');
      }
    }
    return obj;
  }

  /**
   * An array of data fields to save to the Aus Requests.
   * @member {Array.<module:model/AusRequestData>} data
   */
  exports.prototype['data'] = undefined;
  /**
   * @member {String} card_id
   */
  exports.prototype['card_id'] = undefined;
  /**
   * @member {String} card_number
   */
  exports.prototype['card_number'] = undefined;
  /**
   * @member {String} exp_date
   */
  exports.prototype['exp_date'] = undefined;



  return exports;
}));


