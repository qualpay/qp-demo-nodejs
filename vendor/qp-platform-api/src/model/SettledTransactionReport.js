/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.QualpayPlatformApi) {
      root.QualpayPlatformApi = {};
    }
    root.QualpayPlatformApi.SettledTransactionReport = factory(root.QualpayPlatformApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The SettledTransactionReport model module.
   * @module model/SettledTransactionReport
   * @version 1.1.9
   */

  /**
   * Constructs a new <code>SettledTransactionReport</code>.
   * @alias module:model/SettledTransactionReport
   * @class
   */
  var exports = function() {
    var _this = this;































  };

  /**
   * Constructs a <code>SettledTransactionReport</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/SettledTransactionReport} obj Optional instance to populate.
   * @return {module:model/SettledTransactionReport} The populated <code>SettledTransactionReport</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('rec_id')) {
        obj['rec_id'] = ApiClient.convertToType(data['rec_id'], 'Number');
      }
      if (data.hasOwnProperty('merchant_id')) {
        obj['merchant_id'] = ApiClient.convertToType(data['merchant_id'], 'Number');
      }
      if (data.hasOwnProperty('batch_date')) {
        obj['batch_date'] = ApiClient.convertToType(data['batch_date'], 'String');
      }
      if (data.hasOwnProperty('dba_name')) {
        obj['dba_name'] = ApiClient.convertToType(data['dba_name'], 'String');
      }
      if (data.hasOwnProperty('merch_city_ph')) {
        obj['merch_city_ph'] = ApiClient.convertToType(data['merch_city_ph'], 'String');
      }
      if (data.hasOwnProperty('merch_state')) {
        obj['merch_state'] = ApiClient.convertToType(data['merch_state'], 'String');
      }
      if (data.hasOwnProperty('merch_zip')) {
        obj['merch_zip'] = ApiClient.convertToType(data['merch_zip'], 'String');
      }
      if (data.hasOwnProperty('merch_country')) {
        obj['merch_country'] = ApiClient.convertToType(data['merch_country'], 'String');
      }
      if (data.hasOwnProperty('batch_number')) {
        obj['batch_number'] = ApiClient.convertToType(data['batch_number'], 'Number');
      }
      if (data.hasOwnProperty('batch_id')) {
        obj['batch_id'] = ApiClient.convertToType(data['batch_id'], 'Number');
      }
      if (data.hasOwnProperty('card_number')) {
        obj['card_number'] = ApiClient.convertToType(data['card_number'], 'String');
      }
      if (data.hasOwnProperty('card_type')) {
        obj['card_type'] = ApiClient.convertToType(data['card_type'], 'String');
      }
      if (data.hasOwnProperty('auth_date')) {
        obj['auth_date'] = ApiClient.convertToType(data['auth_date'], 'String');
      }
      if (data.hasOwnProperty('tran_date')) {
        obj['tran_date'] = ApiClient.convertToType(data['tran_date'], 'String');
      }
      if (data.hasOwnProperty('settle_date')) {
        obj['settle_date'] = ApiClient.convertToType(data['settle_date'], 'String');
      }
      if (data.hasOwnProperty('amt_auth')) {
        obj['amt_auth'] = ApiClient.convertToType(data['amt_auth'], 'String');
      }
      if (data.hasOwnProperty('amt_tran')) {
        obj['amt_tran'] = ApiClient.convertToType(data['amt_tran'], 'String');
      }
      if (data.hasOwnProperty('tran_currency')) {
        obj['tran_currency'] = ApiClient.convertToType(data['tran_currency'], 'String');
      }
      if (data.hasOwnProperty('reference_number')) {
        obj['reference_number'] = ApiClient.convertToType(data['reference_number'], 'String');
      }
      if (data.hasOwnProperty('purchase_id')) {
        obj['purchase_id'] = ApiClient.convertToType(data['purchase_id'], 'String');
      }
      if (data.hasOwnProperty('auth_code')) {
        obj['auth_code'] = ApiClient.convertToType(data['auth_code'], 'String');
      }
      if (data.hasOwnProperty('moto_ecomm_ind')) {
        obj['moto_ecomm_ind'] = ApiClient.convertToType(data['moto_ecomm_ind'], 'String');
      }
      if (data.hasOwnProperty('pos_entry_mode')) {
        obj['pos_entry_mode'] = ApiClient.convertToType(data['pos_entry_mode'], 'String');
      }
      if (data.hasOwnProperty('amt_funded')) {
        obj['amt_funded'] = ApiClient.convertToType(data['amt_funded'], 'String');
      }
      if (data.hasOwnProperty('funded_currency')) {
        obj['funded_currency'] = ApiClient.convertToType(data['funded_currency'], 'String');
      }
      if (data.hasOwnProperty('pg_id')) {
        obj['pg_id'] = ApiClient.convertToType(data['pg_id'], 'String');
      }
      if (data.hasOwnProperty('merch_ref_num')) {
        obj['merch_ref_num'] = ApiClient.convertToType(data['merch_ref_num'], 'String');
      }
      if (data.hasOwnProperty('acq_reference_number')) {
        obj['acq_reference_number'] = ApiClient.convertToType(data['acq_reference_number'], 'String');
      }
      if (data.hasOwnProperty('auth_rrn')) {
        obj['auth_rrn'] = ApiClient.convertToType(data['auth_rrn'], 'String');
      }
      if (data.hasOwnProperty('auth_tran_id')) {
        obj['auth_tran_id'] = ApiClient.convertToType(data['auth_tran_id'], 'String');
      }
    }
    return obj;
  }

  /**
   * <strong>Format: </strong>Variable length, up to 18 N<br><strong>Description: </strong>Unique ID assigned by Qualpay to this deposit.
   * @member {Number} rec_id
   */
  exports.prototype['rec_id'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 16 N<br><strong>Description: </strong>Unique ID assigned by Qualpay to a Merchant.
   * @member {Number} merchant_id
   */
  exports.prototype['merchant_id'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 10 AN, in YYYY-MM-DD<br><strong>Description: </strong>The date the batch was settled to Qualpay.
   * @member {String} batch_date
   */
  exports.prototype['batch_date'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 25 AN<br><strong>Description: </strong>The doing business as name of the merchant.
   * @member {String} dba_name
   */
  exports.prototype['dba_name'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 13 AN<br><strong>Description: </strong>Sent to the card issuer; either the merchant's city, or phone number.
   * @member {String} merch_city_ph
   */
  exports.prototype['merch_city_ph'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 3 AN<br><strong>Description: </strong>Sent to the card issuer; the merchant's state.
   * @member {String} merch_state
   */
  exports.prototype['merch_state'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 10 AN<br><strong>Description: </strong>Sent to the card issuer; the merchant's ZIP code.
   * @member {String} merch_zip
   */
  exports.prototype['merch_zip'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 3 AN<br><strong>Description: </strong>Sent to the card issuer; the merchant's country code.
   * @member {String} merch_country
   */
  exports.prototype['merch_country'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 3 N<br><strong>Description: </strong>A non-unique ID assigned by the merchant's terminal, POS device, or gateway for this batch, in the range of 1 - 999.
   * @member {Number} batch_number
   */
  exports.prototype['batch_number'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 10 N<br><strong>Description: </strong>Unique ID assigned by Qualpay to this batch.
   * @member {Number} batch_id
   */
  exports.prototype['batch_id'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 16 AN<br><strong>Description: </strong>The truncated card number of the transaction.
   * @member {String} card_number
   */
  exports.prototype['card_number'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 2 AN<br><strong>Description: </strong>The card brand of this transaction.
   * @member {module:model/SettledTransactionReport.CardTypeEnum} card_type
   */
  exports.prototype['card_type'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 10 AN, in YYYY-MM-DD<br><strong>Description: </strong>The date the transaction was authorized by the merchant.
   * @member {String} auth_date
   */
  exports.prototype['auth_date'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 10 AN, in YYYY-MM-DD<br><strong>Description: </strong>The date the transaction was captured by the merchant.
   * @member {String} tran_date
   */
  exports.prototype['tran_date'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 10 AN, in YYYY-MM-DD<br><strong>Description: </strong>The date Qualpay settled the transaction with the issuer.
   * @member {String} settle_date
   */
  exports.prototype['settle_date'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 12,2 N<br><strong>Description: </strong>The amount of the original authorization.
   * @member {String} amt_auth
   */
  exports.prototype['amt_auth'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 12,2 N<br><strong>Description: </strong>The amount of the settled transaction.
   * @member {String} amt_tran
   */
  exports.prototype['amt_tran'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 3 AN<br><strong>Description: </strong>The ISO 4217 numeric currency code of the dispute.
   * @member {String} tran_currency
   */
  exports.prototype['tran_currency'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 23 AN<br><strong>Description: </strong>The bank reference number of the deposit.
   * @member {String} reference_number
   */
  exports.prototype['reference_number'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 25 AN<br><strong>Description: </strong>A merchant supplied tracking number, generally an invoice or purchase number. This number may be visible to the cardholder, depending on card issuer.
   * @member {String} purchase_id
   */
  exports.prototype['purchase_id'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 6 AN<br><strong>Description: </strong>The authorization code provided by the card issuer when the card was approved.
   * @member {String} auth_code
   */
  exports.prototype['auth_code'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 1 AN<br><strong>Description: </strong>The transaction ECI (e-commerce indicator), an indicatior of how the card was processed.
   * @member {String} moto_ecomm_ind
   */
  exports.prototype['moto_ecomm_ind'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 3 AN<br><strong>Description: </strong>Indicates the way a card number was entered. For all codes, please see <a href=\"/developer/api/reference#pos_entry_mode\" target=\"_blank\">POS Entry Modes</a>.
   * @member {String} pos_entry_mode
   */
  exports.prototype['pos_entry_mode'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 12,2 N<br><strong>Description: </strong>The amount of this transaction funded to the merchant (in the funded currency).
   * @member {String} amt_funded
   */
  exports.prototype['amt_funded'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 3 AN<br><strong>Description: </strong>The ISO 4217 numeric currency code of the transaction.
   * @member {String} funded_currency
   */
  exports.prototype['funded_currency'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 32 AN<br><strong>Description: </strong>32-byte unique identifier generated by the payment gateway, returned in all valid responses. Applicable only to Qualpay Payment Gateway transactions. It provides a reference to the transaction and is required for some post-auth operations (e.g. capture, refund, or void).
   * @member {String} pg_id
   */
  exports.prototype['pg_id'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 128 AN<br><strong>Description: </strong>Merchant provided reference value that will be stored with the transaction data and included with transaction data in reports within Qualpay Manager. This value will also be attached to any lifecycle transactions (e.g. retrieval requests and chargebacks) that may occur.
   * @member {String} merch_ref_num
   */
  exports.prototype['merch_ref_num'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 11 N<br><strong>Description: </strong>Acquirer reference number is an 11-digit number generated by the product initiating the transaction. It is a unique number that both the acquirer and the issuer can use to identify a transaction. For chargeback adjustments, the acquirer reference number is used as the deposit reference number.
   * @member {String} acq_reference_number
   */
  exports.prototype['acq_reference_number'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 12 AN<br><strong>Description: </strong>The Authorization Retrieval Reference Number (RRN) is a unique identifier assigned by an acquirer to an authorization.
   * @member {String} auth_rrn
   */
  exports.prototype['auth_rrn'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 15 AN<br><strong>Description: </strong>The Authorization transaction identifier is a unique identifier returned by the issuing bank for an electronically authorized transaction.
   * @member {String} auth_tran_id
   */
  exports.prototype['auth_tran_id'] = undefined;


  /**
   * Allowed values for the <code>card_type</code> property.
   * @enum {String}
   * @readonly
   */
  exports.CardTypeEnum = {
    /**
     * value: "VS"
     * @const
     */
    "VS": "VS",
    /**
     * value: "MC"
     * @const
     */
    "MC": "MC",
    /**
     * value: "DS"
     * @const
     */
    "DS": "DS",
    /**
     * value: "AM"
     * @const
     */
    "AM": "AM"  };


  return exports;
}));


