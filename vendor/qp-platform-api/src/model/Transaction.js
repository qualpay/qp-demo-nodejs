/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.QualpayPlatformApi) {
      root.QualpayPlatformApi = {};
    }
    root.QualpayPlatformApi.Transaction = factory(root.QualpayPlatformApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The Transaction model module.
   * @module model/Transaction
   * @version 1.1.9
   */

  /**
   * Constructs a new <code>Transaction</code>.
   * @alias module:model/Transaction
   * @class
   */
  var exports = function() {
    var _this = this;



























  };

  /**
   * Constructs a <code>Transaction</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Transaction} obj Optional instance to populate.
   * @return {module:model/Transaction} The populated <code>Transaction</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('merchant_id')) {
        obj['merchant_id'] = ApiClient.convertToType(data['merchant_id'], 'Number');
      }
      if (data.hasOwnProperty('tran_time')) {
        obj['tran_time'] = ApiClient.convertToType(data['tran_time'], 'String');
      }
      if (data.hasOwnProperty('tran_date')) {
        obj['tran_date'] = ApiClient.convertToType(data['tran_date'], 'String');
      }
      if (data.hasOwnProperty('tran_status')) {
        obj['tran_status'] = ApiClient.convertToType(data['tran_status'], 'String');
      }
      if (data.hasOwnProperty('auth_code')) {
        obj['auth_code'] = ApiClient.convertToType(data['auth_code'], 'String');
      }
      if (data.hasOwnProperty('rcode')) {
        obj['rcode'] = ApiClient.convertToType(data['rcode'], 'String');
      }
      if (data.hasOwnProperty('card_number')) {
        obj['card_number'] = ApiClient.convertToType(data['card_number'], 'String');
      }
      if (data.hasOwnProperty('card_type')) {
        obj['card_type'] = ApiClient.convertToType(data['card_type'], 'String');
      }
      if (data.hasOwnProperty('purchase_id')) {
        obj['purchase_id'] = ApiClient.convertToType(data['purchase_id'], 'String');
      }
      if (data.hasOwnProperty('pg_id')) {
        obj['pg_id'] = ApiClient.convertToType(data['pg_id'], 'String');
      }
      if (data.hasOwnProperty('cardholder_first_name')) {
        obj['cardholder_first_name'] = ApiClient.convertToType(data['cardholder_first_name'], 'String');
      }
      if (data.hasOwnProperty('cardholder_last_name')) {
        obj['cardholder_last_name'] = ApiClient.convertToType(data['cardholder_last_name'], 'String');
      }
      if (data.hasOwnProperty('amt_tran')) {
        obj['amt_tran'] = ApiClient.convertToType(data['amt_tran'], 'Number');
      }
      if (data.hasOwnProperty('tran_currency')) {
        obj['tran_currency'] = ApiClient.convertToType(data['tran_currency'], 'String');
      }
      if (data.hasOwnProperty('dispute_flag')) {
        obj['dispute_flag'] = ApiClient.convertToType(data['dispute_flag'], 'Boolean');
      }
      if (data.hasOwnProperty('amt_refunded')) {
        obj['amt_refunded'] = ApiClient.convertToType(data['amt_refunded'], 'Number');
      }
      if (data.hasOwnProperty('batch_number')) {
        obj['batch_number'] = ApiClient.convertToType(data['batch_number'], 'Number');
      }
      if (data.hasOwnProperty('dba_name')) {
        obj['dba_name'] = ApiClient.convertToType(data['dba_name'], 'String');
      }
      if (data.hasOwnProperty('customer_id')) {
        obj['customer_id'] = ApiClient.convertToType(data['customer_id'], 'String');
      }
      if (data.hasOwnProperty('subscription_id')) {
        obj['subscription_id'] = ApiClient.convertToType(data['subscription_id'], 'String');
      }
      if (data.hasOwnProperty('merch_ref_num')) {
        obj['merch_ref_num'] = ApiClient.convertToType(data['merch_ref_num'], 'String');
      }
      if (data.hasOwnProperty('batch_id')) {
        obj['batch_id'] = ApiClient.convertToType(data['batch_id'], 'Number');
      }
      if (data.hasOwnProperty('batch_date')) {
        obj['batch_date'] = ApiClient.convertToType(data['batch_date'], 'String');
      }
      if (data.hasOwnProperty('settle_date')) {
        obj['settle_date'] = ApiClient.convertToType(data['settle_date'], 'String');
      }
      if (data.hasOwnProperty('amt_funded')) {
        obj['amt_funded'] = ApiClient.convertToType(data['amt_funded'], 'Number');
      }
      if (data.hasOwnProperty('funded_currency')) {
        obj['funded_currency'] = ApiClient.convertToType(data['funded_currency'], 'String');
      }
    }
    return obj;
  }

  /**
   * <strong>Format: </strong>Variable length, up to 16 AN<br><strong>Description: </strong>Identifies the merchant to which this transaction belongs.
   * @member {Number} merchant_id
   */
  exports.prototype['merchant_id'] = undefined;
  /**
   * <strong>Format: </strong>Variable length,  in YYYY-MM-DD HH:MM:ss format<br><strong>Description: </strong>Transaction time. All times are Pacific time. 
   * @member {String} tran_time
   */
  exports.prototype['tran_time'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 10 AN, YYYY-MM-DD format<br><strong>Description: </strong>The date the transaction was captured by the merchant.
   * @member {String} tran_date
   */
  exports.prototype['tran_date'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 1 AN<br><strong>Description: </strong>Transaction status.<ul><li>A - Transaction is approved</li><li>H - Transaction Held</li><li>C - Transaction is captured</li><li>V - Transaction is voided by Merchant</li><li>v - Transaction is voided by System</li><li>K - Transaction is cancelled</li><li>D - Transaction is declined by issuer</li><li>F - Transaction failures other than Issuer Declines</li><li>S - Transaction Settled</li><li>P - Deposit Sent</li><li>N - Transaction Settled, but will not be funded by Qualpay</li><li>R - Transaction Rejected</li></ul>
   * @member {String} tran_status
   */
  exports.prototype['tran_status'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 6 AN<br><strong>Description: </strong>Authorization code from issuer.
   * @member {String} auth_code
   */
  exports.prototype['auth_code'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 3 AN<br><strong>Description: </strong>Gateway response code. Refer to <a href=\"/developer/api/reference#gateway-response-codes\"target=\"_blank\">Payment Gateway Response Codes</a> for possible values.
   * @member {String} rcode
   */
  exports.prototype['rcode'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 16 AN<br><strong>Description: </strong>Masked card number. 
   * @member {String} card_number
   */
  exports.prototype['card_number'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 2 AN<br><strong>Description: </strong>Card type of the billing card used for the transaction. Refer to <a href=\"/developer/api/reference#card-types\"target=\"_blank\">Card Types</a> for possible values. 
   * @member {module:model/Transaction.CardTypeEnum} card_type
   */
  exports.prototype['card_type'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 25 AN<br><strong>Description: </strong>Purchase ID of the transaction.
   * @member {String} purchase_id
   */
  exports.prototype['purchase_id'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 32 AN<br><strong>Description: </strong>Qualpay generated payment gateway ID for the transaction.
   * @member {String} pg_id
   */
  exports.prototype['pg_id'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 32 AN<br><strong>Description: </strong>First name of card holder.
   * @member {String} cardholder_first_name
   */
  exports.prototype['cardholder_first_name'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 32 AN<br><strong>Description: </strong>Last name of card holder.
   * @member {String} cardholder_last_name
   */
  exports.prototype['cardholder_last_name'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 12,2 N<br><strong>Description: </strong>Transaction amount.
   * @member {Number} amt_tran
   */
  exports.prototype['amt_tran'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 3 AN<br><strong>Description: </strong>Numeric currency code of the transaction. Refer to <a href=\"/developer/api/reference#country-codes\"target=\"_blank\">Country Codes</a> for a list of currency codes. 
   * @member {String} tran_currency
   */
  exports.prototype['tran_currency'] = undefined;
  /**
   * <br><strong>Description: </strong>Will be set to true if the transaction is disputed.
   * @member {Boolean} dispute_flag
   */
  exports.prototype['dispute_flag'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 12,2 N<br><strong>Description: </strong>Amount refunded if there are any refunds.
   * @member {Number} amt_refunded
   */
  exports.prototype['amt_refunded'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 3 N<br><strong>Description: </strong>Settlement batch number.
   * @member {Number} batch_number
   */
  exports.prototype['batch_number'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 25 AN<br><strong>Description: </strong>DBA name used in the transaction.
   * @member {String} dba_name
   */
  exports.prototype['dba_name'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 32 AN<br><strong>Description: </strong>Customer vault ID.
   * @member {String} customer_id
   */
  exports.prototype['customer_id'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 10 N<br><strong>Description: </strong>Subscription identifier.
   * @member {String} subscription_id
   */
  exports.prototype['subscription_id'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 128 AN<br><strong>Description: </strong>Merchant provided reference number for this transaction.
   * @member {String} merch_ref_num
   */
  exports.prototype['merch_ref_num'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 10 N<br><strong>Description: </strong>Unique ID assigned by Qualpay to this batch.
   * @member {Number} batch_id
   */
  exports.prototype['batch_id'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 10, YYYY-MM-DD format<br><strong>Description: </strong>The date the batch was closed.
   * @member {String} batch_date
   */
  exports.prototype['batch_date'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 10, YYYY-MM-DD format<br><strong>Description: </strong>The date Qualpay settled the transaction with the issuer.
   * @member {String} settle_date
   */
  exports.prototype['settle_date'] = undefined;
  /**
   * <strong>Format: </strong>Variable length, up to 10,2 N<br><strong>Description: </strong>The amount funded to the merchant (in funded currency).
   * @member {Number} amt_funded
   */
  exports.prototype['amt_funded'] = undefined;
  /**
   * <strong>Format: </strong>Fixed length, 3 AN<br><strong>Description: </strong>Numeric currency code of the funded amount. Refer to <a href=\"/developer/api/reference#country-codes\"target=\"_blank\">Country Codes</a> for possible values. 
   * @member {String} funded_currency
   */
  exports.prototype['funded_currency'] = undefined;


  /**
   * Allowed values for the <code>card_type</code> property.
   * @enum {String}
   * @readonly
   */
  exports.CardTypeEnum = {
    /**
     * value: "VS"
     * @const
     */
    "VS": "VS",
    /**
     * value: "MC"
     * @const
     */
    "MC": "MC",
    /**
     * value: "AM"
     * @const
     */
    "AM": "AM",
    /**
     * value: "DS"
     * @const
     */
    "DS": "DS",
    /**
     * value: "JC"
     * @const
     */
    "JC": "JC",
    /**
     * value: "PP"
     * @const
     */
    "PP": "PP"  };


  return exports;
}));


