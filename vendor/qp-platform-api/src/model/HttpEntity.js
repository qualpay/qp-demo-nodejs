/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.QualpayPlatformApi) {
      root.QualpayPlatformApi = {};
    }
    root.QualpayPlatformApi.HttpEntity = factory(root.QualpayPlatformApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The HttpEntity model module.
   * @module model/HttpEntity
   * @version 1.1.9
   */

  /**
   * Constructs a new <code>HttpEntity</code>.
   * @alias module:model/HttpEntity
   * @class
   * @param knownEmpty {Boolean} 
   */
  var exports = function(knownEmpty) {
    var _this = this;

    _this['knownEmpty'] = knownEmpty;
  };

  /**
   * Constructs a <code>HttpEntity</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/HttpEntity} obj Optional instance to populate.
   * @return {module:model/HttpEntity} The populated <code>HttpEntity</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('knownEmpty')) {
        obj['knownEmpty'] = ApiClient.convertToType(data['knownEmpty'], 'Boolean');
      }
    }
    return obj;
  }

  /**
   * @member {Boolean} knownEmpty
   */
  exports.prototype['knownEmpty'] = undefined;



  return exports;
}));


