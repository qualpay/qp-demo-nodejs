/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ShippingAddress'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./ShippingAddress'));
  } else {
    // Browser globals (root is window)
    if (!root.QualpayPlatformApi) {
      root.QualpayPlatformApi = {};
    }
    root.QualpayPlatformApi.GetShippingAddressesResponse = factory(root.QualpayPlatformApi.ApiClient, root.QualpayPlatformApi.ShippingAddress);
  }
}(this, function(ApiClient, ShippingAddress) {
  'use strict';




  /**
   * The GetShippingAddressesResponse model module.
   * @module model/GetShippingAddressesResponse
   * @version 1.1.9
   */

  /**
   * Constructs a new <code>GetShippingAddressesResponse</code>.
   * @alias module:model/GetShippingAddressesResponse
   * @class
   */
  var exports = function() {
    var _this = this;


  };

  /**
   * Constructs a <code>GetShippingAddressesResponse</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/GetShippingAddressesResponse} obj Optional instance to populate.
   * @return {module:model/GetShippingAddressesResponse} The populated <code>GetShippingAddressesResponse</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('shipping_addresses')) {
        obj['shipping_addresses'] = ApiClient.convertToType(data['shipping_addresses'], [ShippingAddress]);
      }
    }
    return obj;
  }

  /**
   * <br><strong>Description: </strong>An array of shipping addresses. 
   * @member {Array.<module:model/ShippingAddress>} shipping_addresses
   */
  exports.prototype['shipping_addresses'] = undefined;



  return exports;
}));


