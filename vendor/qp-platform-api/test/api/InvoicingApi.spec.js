/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.QualpayPlatformApi);
  }
}(this, function(expect, QualpayPlatformApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new QualpayPlatformApi.InvoicingApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('InvoicingApi', function() {
    describe('addInvoicePayment', function() {
      it('should call addInvoicePayment successfully', function(done) {
        //uncomment below and update the code to test addInvoicePayment
        //instance.addInvoicePayment(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('browseBouncedInvoices', function() {
      it('should call browseBouncedInvoices successfully', function(done) {
        //uncomment below and update the code to test browseBouncedInvoices
        //instance.browseBouncedInvoices(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('browseInvoicePayments', function() {
      it('should call browseInvoicePayments successfully', function(done) {
        //uncomment below and update the code to test browseInvoicePayments
        //instance.browseInvoicePayments(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('browseInvoicePaymentsById', function() {
      it('should call browseInvoicePaymentsById successfully', function(done) {
        //uncomment below and update the code to test browseInvoicePaymentsById
        //instance.browseInvoicePaymentsById(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('browseInvoices', function() {
      it('should call browseInvoices successfully', function(done) {
        //uncomment below and update the code to test browseInvoices
        //instance.browseInvoices(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('cancelInvoice', function() {
      it('should call cancelInvoice successfully', function(done) {
        //uncomment below and update the code to test cancelInvoice
        //instance.cancelInvoice(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('copyInvoice', function() {
      it('should call copyInvoice successfully', function(done) {
        //uncomment below and update the code to test copyInvoice
        //instance.copyInvoice(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('createInvoice', function() {
      it('should call createInvoice successfully', function(done) {
        //uncomment below and update the code to test createInvoice
        //instance.createInvoice(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getInvoice', function() {
      it('should call getInvoice successfully', function(done) {
        //uncomment below and update the code to test getInvoice
        //instance.getInvoice(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('removeInvoicePayment', function() {
      it('should call removeInvoicePayment successfully', function(done) {
        //uncomment below and update the code to test removeInvoicePayment
        //instance.removeInvoicePayment(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('resendInvoice', function() {
      it('should call resendInvoice successfully', function(done) {
        //uncomment below and update the code to test resendInvoice
        //instance.resendInvoice(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('sendInvoice', function() {
      it('should call sendInvoice successfully', function(done) {
        //uncomment below and update the code to test sendInvoice
        //instance.sendInvoice(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('updateDraftInvoice', function() {
      it('should call updateDraftInvoice successfully', function(done) {
        //uncomment below and update the code to test updateDraftInvoice
        //instance.updateDraftInvoice(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('updateInvoicePayment', function() {
      it('should call updateInvoicePayment successfully', function(done) {
        //uncomment below and update the code to test updateInvoicePayment
        //instance.updateInvoicePayment(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('updateOutstandingInvoice', function() {
      it('should call updateOutstandingInvoice successfully', function(done) {
        //uncomment below and update the code to test updateOutstandingInvoice
        //instance.updateOutstandingInvoice(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
