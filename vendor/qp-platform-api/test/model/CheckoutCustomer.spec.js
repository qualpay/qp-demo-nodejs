/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.QualpayPlatformApi);
  }
}(this, function(expect, QualpayPlatformApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new QualpayPlatformApi.CheckoutCustomer();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('CheckoutCustomer', function() {
    it('should create an instance of CheckoutCustomer', function() {
      // uncomment below and update the code to test CheckoutCustomer
      //var instance = new QualpayPlatformApi.CheckoutCustomer();
      //expect(instance).to.be.a(QualpayPlatformApi.CheckoutCustomer);
    });

    it('should have the property customerFirstName (base name: "customer_first_name")', function() {
      // uncomment below and update the code to test the property customerFirstName
      //var instance = new QualpayPlatformApi.CheckoutCustomer();
      //expect(instance).to.be();
    });

    it('should have the property customerLastName (base name: "customer_last_name")', function() {
      // uncomment below and update the code to test the property customerLastName
      //var instance = new QualpayPlatformApi.CheckoutCustomer();
      //expect(instance).to.be();
    });

    it('should have the property customerEmail (base name: "customer_email")', function() {
      // uncomment below and update the code to test the property customerEmail
      //var instance = new QualpayPlatformApi.CheckoutCustomer();
      //expect(instance).to.be();
    });

    it('should have the property billingAddr1 (base name: "billing_addr1")', function() {
      // uncomment below and update the code to test the property billingAddr1
      //var instance = new QualpayPlatformApi.CheckoutCustomer();
      //expect(instance).to.be();
    });

    it('should have the property billingCity (base name: "billing_city")', function() {
      // uncomment below and update the code to test the property billingCity
      //var instance = new QualpayPlatformApi.CheckoutCustomer();
      //expect(instance).to.be();
    });

    it('should have the property billingState (base name: "billing_state")', function() {
      // uncomment below and update the code to test the property billingState
      //var instance = new QualpayPlatformApi.CheckoutCustomer();
      //expect(instance).to.be();
    });

    it('should have the property billingZip (base name: "billing_zip")', function() {
      // uncomment below and update the code to test the property billingZip
      //var instance = new QualpayPlatformApi.CheckoutCustomer();
      //expect(instance).to.be();
    });

  });

}));
