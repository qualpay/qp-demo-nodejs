/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.QualpayPlatformApi);
  }
}(this, function(expect, QualpayPlatformApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new QualpayPlatformApi.RotateKeyRequest();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('RotateKeyRequest', function() {
    it('should create an instance of RotateKeyRequest', function() {
      // uncomment below and update the code to test RotateKeyRequest
      //var instance = new QualpayPlatformApi.RotateKeyRequest();
      //expect(instance).to.be.a(QualpayPlatformApi.RotateKeyRequest);
    });

    it('should have the property minutes (base name: "minutes")', function() {
      // uncomment below and update the code to test the property minutes
      //var instance = new QualpayPlatformApi.RotateKeyRequest();
      //expect(instance).to.be();
    });

  });

}));
