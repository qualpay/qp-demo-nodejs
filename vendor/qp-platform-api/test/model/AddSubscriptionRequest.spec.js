/**
 * Qualpay Platform API
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.2
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.QualpayPlatformApi);
  }
}(this, function(expect, QualpayPlatformApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new QualpayPlatformApi.AddSubscriptionRequest();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('AddSubscriptionRequest', function() {
    it('should create an instance of AddSubscriptionRequest', function() {
      // uncomment below and update the code to test AddSubscriptionRequest
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be.a(QualpayPlatformApi.AddSubscriptionRequest);
    });

    it('should have the property dateStart (base name: "date_start")', function() {
      // uncomment below and update the code to test the property dateStart
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be();
    });

    it('should have the property customerId (base name: "customer_id")', function() {
      // uncomment below and update the code to test the property customerId
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be();
    });

    it('should have the property customer (base name: "customer")', function() {
      // uncomment below and update the code to test the property customer
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be();
    });

    it('should have the property planCode (base name: "plan_code")', function() {
      // uncomment below and update the code to test the property planCode
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be();
    });

    it('should have the property planDesc (base name: "plan_desc")', function() {
      // uncomment below and update the code to test the property planDesc
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be();
    });

    it('should have the property planFrequency (base name: "plan_frequency")', function() {
      // uncomment below and update the code to test the property planFrequency
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be();
    });

    it('should have the property interval (base name: "interval")', function() {
      // uncomment below and update the code to test the property interval
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be();
    });

    it('should have the property planDuration (base name: "plan_duration")', function() {
      // uncomment below and update the code to test the property planDuration
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be();
    });

    it('should have the property amtSetup (base name: "amt_setup")', function() {
      // uncomment below and update the code to test the property amtSetup
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be();
    });

    it('should have the property amtTran (base name: "amt_tran")', function() {
      // uncomment below and update the code to test the property amtTran
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be();
    });

    it('should have the property profileId (base name: "profile_id")', function() {
      // uncomment below and update the code to test the property profileId
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be();
    });

    it('should have the property tranCurrency (base name: "tran_currency")', function() {
      // uncomment below and update the code to test the property tranCurrency
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be();
    });

    it('should have the property useExistingCustomer (base name: "use_existing_customer")', function() {
      // uncomment below and update the code to test the property useExistingCustomer
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be();
    });

    it('should have the property merchantId (base name: "merchant_id")', function() {
      // uncomment below and update the code to test the property merchantId
      //var instance = new QualpayPlatformApi.AddSubscriptionRequest();
      //expect(instance).to.be();
    });

  });

}));
