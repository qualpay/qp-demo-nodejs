# qualpay_platform_api

QualpayPlatformApi - JavaScript client for qualpay_platform_api
This document describes the Qualpay Platform API.
This SDK is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 1.1.9
- Package version: 1.1.9
- Build package: io.swagger.codegen.languages.JavascriptClientCodegen

## Installation

### For [Node.js](https://nodejs.org/)

#### npm

To publish the library as a [npm](https://www.npmjs.com/),
please follow the procedure in ["Publishing npm packages"](https://docs.npmjs.com/getting-started/publishing-npm-packages).

Then install it via:

```shell
npm install qualpay_platform_api --save
```

##### Local development

To use the library locally without publishing to a remote npm registry, first install the dependencies by changing 
into the directory containing `package.json` (and this README). Let's call this `JAVASCRIPT_CLIENT_DIR`. Then run:

```shell
npm install
```

Next, [link](https://docs.npmjs.com/cli/link) it globally in npm with the following, also from `JAVASCRIPT_CLIENT_DIR`:

```shell
npm link
```

Finally, switch to the directory you want to use your qualpay_platform_api from, and run:

```shell
npm link /path/to/<JAVASCRIPT_CLIENT_DIR>
```

You should now be able to `require('qualpay_platform_api')` in javascript files from the directory you ran the last 
command above from.

#### git
#
If the library is hosted at a git repository, e.g.
https://github.com/YOUR_USERNAME/qualpay_platform_api
then install it via:

```shell
    npm install YOUR_USERNAME/qualpay_platform_api --save
```

### For browser

The library also works in the browser environment via npm and [browserify](http://browserify.org/). After following
the above steps with Node.js and installing browserify with `npm install -g browserify`,
perform the following (assuming *main.js* is your entry file, that's to say your javascript file where you actually 
use this library):

```shell
browserify main.js > bundle.js
```

Then include *bundle.js* in the HTML pages.

### Webpack Configuration

Using Webpack you may encounter the following error: "Module not found: Error:
Cannot resolve module", most certainly you should disable AMD loader. Add/merge
the following section to your webpack config:

```javascript
module: {
  rules: [
    {
      parser: {
        amd: false
      }
    }
  ]
}
```

## Getting Started

Please follow the [installation](#installation) instruction and execute the following JS code:

```javascript
var QualpayPlatformApi = require('qualpay_platform_api');

var defaultClient = QualpayPlatformApi.ApiClient.instance;

// Configure HTTP basic authorization: basicAuth
var basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME'
basicAuth.password = 'YOUR PASSWORD'

var api = new QualpayPlatformApi.AccountUpdaterApi()

var file = "/path/to/file.txt"; // {File} The file to upload.


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.addAusFileRequest(file, callback);

```

## Documentation for API Endpoints

All URIs are relative to *https://api-test.qualpay.com/platform*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*QualpayPlatformApi.AccountUpdaterApi* | [**addAusFileRequest**](docs/AccountUpdaterApi.md#addAusFileRequest) | **POST** /aus/add/file | Submit AUS CSV File Request
*QualpayPlatformApi.AccountUpdaterApi* | [**addAusJsonRequest**](docs/AccountUpdaterApi.md#addAusJsonRequest) | **POST** /aus/add | Submit AUS Request
*QualpayPlatformApi.AccountUpdaterApi* | [**getAusResponse**](docs/AccountUpdaterApi.md#getAusResponse) | **GET** /aus/{requestId} | Get AUS Response
*QualpayPlatformApi.ApplicationBoardingApi* | [**addApp**](docs/ApplicationBoardingApi.md#addApp) | **POST** /application/add | Create Application
*QualpayPlatformApi.ApplicationBoardingApi* | [**browseApps**](docs/ApplicationBoardingApi.md#browseApps) | **GET** /application/browse | Browse applications
*QualpayPlatformApi.ApplicationBoardingApi* | [**browseSalesReps**](docs/ApplicationBoardingApi.md#browseSalesReps) | **GET** /application/sales/browse | Browse Sales Reps
*QualpayPlatformApi.ApplicationBoardingApi* | [**emailApp**](docs/ApplicationBoardingApi.md#emailApp) | **POST** /application/{appId}/email | Email Application
*QualpayPlatformApi.ApplicationBoardingApi* | [**getApp**](docs/ApplicationBoardingApi.md#getApp) | **GET** /application/{appId}/get | Get Application
*QualpayPlatformApi.ApplicationBoardingApi* | [**saveData**](docs/ApplicationBoardingApi.md#saveData) | **POST** /application/{appId}/data | Save Data
*QualpayPlatformApi.ApplicationBoardingApi* | [**uploadFile**](docs/ApplicationBoardingApi.md#uploadFile) | **POST** /application/{appId}/upload | Upload Document
*QualpayPlatformApi.ApplicationBoardingApi* | [**validateApp**](docs/ApplicationBoardingApi.md#validateApp) | **GET** /application/{appId}/validate | Validate Application
*QualpayPlatformApi.CustomerVaultApi* | [**addBillingCard**](docs/CustomerVaultApi.md#addBillingCard) | **POST** /vault/customer/{customer_id}/billing | Add a Billing Card
*QualpayPlatformApi.CustomerVaultApi* | [**addCustomer**](docs/CustomerVaultApi.md#addCustomer) | **POST** /vault/customer | Add a Customer
*QualpayPlatformApi.CustomerVaultApi* | [**addShippingAddress**](docs/CustomerVaultApi.md#addShippingAddress) | **POST** /vault/customer/{customer_id}/shipping | Add a Shipping Address
*QualpayPlatformApi.CustomerVaultApi* | [**browseCustomers**](docs/CustomerVaultApi.md#browseCustomers) | **GET** /vault/customer | Get all Customers
*QualpayPlatformApi.CustomerVaultApi* | [**deleteBillingCard**](docs/CustomerVaultApi.md#deleteBillingCard) | **PUT** /vault/customer/{customer_id}/billing/delete | Delete a Billing Card
*QualpayPlatformApi.CustomerVaultApi* | [**deleteCustomer**](docs/CustomerVaultApi.md#deleteCustomer) | **DELETE** /vault/customer/{customer_id} | Delete a Customer
*QualpayPlatformApi.CustomerVaultApi* | [**deleteShippingAddress**](docs/CustomerVaultApi.md#deleteShippingAddress) | **DELETE** /vault/customer/{customer_id}/shipping/{id} | Delete a Shipping Address
*QualpayPlatformApi.CustomerVaultApi* | [**getBillingCards**](docs/CustomerVaultApi.md#getBillingCards) | **GET** /vault/customer/{customer_id}/billing | Get Billing Cards
*QualpayPlatformApi.CustomerVaultApi* | [**getCustomer**](docs/CustomerVaultApi.md#getCustomer) | **GET** /vault/customer/{customer_id} | Get by Customer ID
*QualpayPlatformApi.CustomerVaultApi* | [**getShippingAddresses**](docs/CustomerVaultApi.md#getShippingAddresses) | **GET** /vault/customer/{customer_id}/shipping | Get Shipping Addresses
*QualpayPlatformApi.CustomerVaultApi* | [**setPrimaryBillingCard**](docs/CustomerVaultApi.md#setPrimaryBillingCard) | **PUT** /vault/customer/{customer_id}/billing/primary | Set Primary Billing Card
*QualpayPlatformApi.CustomerVaultApi* | [**setPrimaryShippingAddress**](docs/CustomerVaultApi.md#setPrimaryShippingAddress) | **PUT** /vault/customer/{customer_id}/shipping/primary | Set Primary Shipping Address
*QualpayPlatformApi.CustomerVaultApi* | [**updateBillingCard**](docs/CustomerVaultApi.md#updateBillingCard) | **PUT** /vault/customer/{customer_id}/billing | Update a Billing Card
*QualpayPlatformApi.CustomerVaultApi* | [**updateCustomer**](docs/CustomerVaultApi.md#updateCustomer) | **PUT** /vault/customer/{customer_id} | Update a Customer
*QualpayPlatformApi.CustomerVaultApi* | [**updateShippingAddress**](docs/CustomerVaultApi.md#updateShippingAddress) | **PUT** /vault/customer/{customer_id}/shipping | Update a Shipping Address
*QualpayPlatformApi.DisputeManagementApi* | [**acceptDispute**](docs/DisputeManagementApi.md#acceptDispute) | **GET** /dispute/{recId}/accept | Accept Dispute
*QualpayPlatformApi.DisputeManagementApi* | [**browseDisputes**](docs/DisputeManagementApi.md#browseDisputes) | **GET** /dispute/browse | Get Disputes
*QualpayPlatformApi.DisputeManagementApi* | [**createDispute**](docs/DisputeManagementApi.md#createDispute) | **GET** /dispute/{merchantId}/create | Create Dispute Data
*QualpayPlatformApi.DisputeManagementApi* | [**getCreditedTran**](docs/DisputeManagementApi.md#getCreditedTran) | **GET** /dispute/{recId}/creditedtrans | Get Credited Transaction Detail
*QualpayPlatformApi.DisputeManagementApi* | [**getDisputeResponse**](docs/DisputeManagementApi.md#getDisputeResponse) | **GET** /dispute/{recId}/response | Get Submitted Dispute Response
*QualpayPlatformApi.DisputeManagementApi* | [**getNonDisputedTran**](docs/DisputeManagementApi.md#getNonDisputedTran) | **GET** /dispute/{recId}/nondisputedtrans | Get Non Disputed Transaction 
*QualpayPlatformApi.DisputeManagementApi* | [**resetDispute**](docs/DisputeManagementApi.md#resetDispute) | **GET** /dispute/{recId}/reset | Reset Dispute Data
*QualpayPlatformApi.DisputeManagementApi* | [**submitDisputesResponse**](docs/DisputeManagementApi.md#submitDisputesResponse) | **POST** /dispute/{recId}/respond | Submit Dispute Response
*QualpayPlatformApi.EmbeddedFieldsApi* | [**getEmbeddedTransientKey**](docs/EmbeddedFieldsApi.md#getEmbeddedTransientKey) | **GET** /embedded | Get Transient Key
*QualpayPlatformApi.IntegratorApi* | [**createTestAccount**](docs/IntegratorApi.md#createTestAccount) | **POST** /vendor/settings/account/add | Generate Test Account
*QualpayPlatformApi.IntegratorApi* | [**getMerchantSettings**](docs/IntegratorApi.md#getMerchantSettings) | **GET** /vendor/settings/{mid} | Merchant Settings
*QualpayPlatformApi.IntegratorApi* | [**rotateApiKey**](docs/IntegratorApi.md#rotateApiKey) | **POST** /vendor/settings/key/{mid} | Generate Security Key
*QualpayPlatformApi.InvoicingApi* | [**addInvoicePayment**](docs/InvoicingApi.md#addInvoicePayment) | **POST** /invoice/{invoice_id}/payments | Add Payment to an Invoice
*QualpayPlatformApi.InvoicingApi* | [**browseBouncedInvoices**](docs/InvoicingApi.md#browseBouncedInvoices) | **GET** /invoice/bounced | Get Undelivered Invoices
*QualpayPlatformApi.InvoicingApi* | [**browseInvoicePayments**](docs/InvoicingApi.md#browseInvoicePayments) | **GET** /invoice/payments | Get Invoice Payments
*QualpayPlatformApi.InvoicingApi* | [**browseInvoicePaymentsById**](docs/InvoicingApi.md#browseInvoicePaymentsById) | **GET** /invoice/{invoice_id}/payments | Get invoice payments by id
*QualpayPlatformApi.InvoicingApi* | [**browseInvoices**](docs/InvoicingApi.md#browseInvoices) | **GET** /invoice | Get all Invoices
*QualpayPlatformApi.InvoicingApi* | [**cancelInvoice**](docs/InvoicingApi.md#cancelInvoice) | **DELETE** /invoice/{invoice_id}/cancel | Cancel an Invoice
*QualpayPlatformApi.InvoicingApi* | [**copyInvoice**](docs/InvoicingApi.md#copyInvoice) | **POST** /invoice/{invoice_id}/copy | Copy an Invoice
*QualpayPlatformApi.InvoicingApi* | [**createInvoice**](docs/InvoicingApi.md#createInvoice) | **POST** /invoice | Create an invoice
*QualpayPlatformApi.InvoicingApi* | [**getInvoice**](docs/InvoicingApi.md#getInvoice) | **GET** /invoice/{invoice_id}/detail | Get by Invoice ID
*QualpayPlatformApi.InvoicingApi* | [**removeInvoicePayment**](docs/InvoicingApi.md#removeInvoicePayment) | **DELETE** /invoice/{invoice_id}/payments/{payment_id} | Remove an Invoice Payment
*QualpayPlatformApi.InvoicingApi* | [**resendInvoice**](docs/InvoicingApi.md#resendInvoice) | **POST** /invoice/{invoice_id}/resend | Resend an Invoice
*QualpayPlatformApi.InvoicingApi* | [**sendInvoice**](docs/InvoicingApi.md#sendInvoice) | **POST** /invoice/{invoice_id}/send | Send an Invoice
*QualpayPlatformApi.InvoicingApi* | [**updateDraftInvoice**](docs/InvoicingApi.md#updateDraftInvoice) | **PUT** /invoice/{invoice_id}/draft | Update a Draft Invoice
*QualpayPlatformApi.InvoicingApi* | [**updateInvoicePayment**](docs/InvoicingApi.md#updateInvoicePayment) | **PUT** /invoice/{invoice_id}/payments/{payment_id} | Update an Invoice Payment
*QualpayPlatformApi.InvoicingApi* | [**updateOutstandingInvoice**](docs/InvoicingApi.md#updateOutstandingInvoice) | **PUT** /invoice/{invoice_id}/outstanding | Update an Outstanding Invoice
*QualpayPlatformApi.QualpayCheckoutApi* | [**addCheckout**](docs/QualpayCheckoutApi.md#addCheckout) | **POST** /checkout | Create a Checkout Payment Link
*QualpayPlatformApi.QualpayCheckoutApi* | [**getDetails**](docs/QualpayCheckoutApi.md#getDetails) | **GET** /checkout/{checkoutId} | Lookup Checkout Payment
*QualpayPlatformApi.RecurringBillingApi* | [**addPlan**](docs/RecurringBillingApi.md#addPlan) | **POST** /plan | Add a Recurring Plan
*QualpayPlatformApi.RecurringBillingApi* | [**addSubscription**](docs/RecurringBillingApi.md#addSubscription) | **POST** /subscription | Add a Subscription
*QualpayPlatformApi.RecurringBillingApi* | [**archivePlan**](docs/RecurringBillingApi.md#archivePlan) | **POST** /plan/{plan_code}/archive | Archive a Recurring Plan
*QualpayPlatformApi.RecurringBillingApi* | [**browsePlans**](docs/RecurringBillingApi.md#browsePlans) | **GET** /plan | Get all Recurring Plans
*QualpayPlatformApi.RecurringBillingApi* | [**browseSubscriptions**](docs/RecurringBillingApi.md#browseSubscriptions) | **GET** /subscription | Get all Subscriptions
*QualpayPlatformApi.RecurringBillingApi* | [**cancelSubscription**](docs/RecurringBillingApi.md#cancelSubscription) | **POST** /subscription/{subscription_id}/cancel | Cancel a Subscription
*QualpayPlatformApi.RecurringBillingApi* | [**deletePlan**](docs/RecurringBillingApi.md#deletePlan) | **DELETE** /plan/{plan_id}/delete | Delete a Recurring Plan
*QualpayPlatformApi.RecurringBillingApi* | [**getAllSubscriptionTransactions**](docs/RecurringBillingApi.md#getAllSubscriptionTransactions) | **GET** /subscription/transactions | Get all subscription transactions
*QualpayPlatformApi.RecurringBillingApi* | [**getPlan**](docs/RecurringBillingApi.md#getPlan) | **GET** /plan/{plan_code} | Find Recurring Plan by Plan Code
*QualpayPlatformApi.RecurringBillingApi* | [**getSubscription**](docs/RecurringBillingApi.md#getSubscription) | **GET** /subscription/{subscription_id} | Get Subscription by Subscription ID
*QualpayPlatformApi.RecurringBillingApi* | [**getSubscriptionTransactions**](docs/RecurringBillingApi.md#getSubscriptionTransactions) | **GET** /subscription/transactions/{subscription_id} | Get transactions by Subscription ID
*QualpayPlatformApi.RecurringBillingApi* | [**pauseSubscription**](docs/RecurringBillingApi.md#pauseSubscription) | **POST** /subscription/{subscription_id}/pause | Pause a Subscription
*QualpayPlatformApi.RecurringBillingApi* | [**resumeSubscription**](docs/RecurringBillingApi.md#resumeSubscription) | **POST** /subscription/{subscription_id}/resume | Resume a Subscription
*QualpayPlatformApi.RecurringBillingApi* | [**updatePlan**](docs/RecurringBillingApi.md#updatePlan) | **PUT** /plan/{plan_code} | Update a Recurring Plan
*QualpayPlatformApi.RecurringBillingApi* | [**updateSubscription**](docs/RecurringBillingApi.md#updateSubscription) | **PUT** /subscription/{subscription_id} | Update a Subscription
*QualpayPlatformApi.ReportingApi* | [**browseAusRequests**](docs/ReportingApi.md#browseAusRequests) | **GET** /reporting/aus/detail | Account Updater Detail Report
*QualpayPlatformApi.ReportingApi* | [**browseAusSummary**](docs/ReportingApi.md#browseAusSummary) | **GET** /reporting/aus/summary | Account Updater Summary Report
*QualpayPlatformApi.ReportingApi* | [**browseBatches**](docs/ReportingApi.md#browseBatches) | **GET** /reporting/batches | Batch Report
*QualpayPlatformApi.ReportingApi* | [**browseDeposits**](docs/ReportingApi.md#browseDeposits) | **GET** /reporting/deposits | Deposit Report
*QualpayPlatformApi.ReportingApi* | [**browseDisputes**](docs/ReportingApi.md#browseDisputes) | **GET** /reporting/disputes | Disputes Report
*QualpayPlatformApi.ReportingApi* | [**browseTrans**](docs/ReportingApi.md#browseTrans) | **GET** /reporting/transactions | Settled Transaction Report
*QualpayPlatformApi.ReportingApi* | [**getTransactionByPgId**](docs/ReportingApi.md#getTransactionByPgId) | **GET** /reporting/transactions/bypgid/{pg_id} | Get transaction by PG ID
*QualpayPlatformApi.ReportingApi* | [**getTransactionRequests**](docs/ReportingApi.md#getTransactionRequests) | **GET** /reporting/transaction-requests | Transaction Report
*QualpayPlatformApi.WebhooksApi* | [**addEvent**](docs/WebhooksApi.md#addEvent) | **POST** /webhook/{webhook_id}/event/{event} | Add an event
*QualpayPlatformApi.WebhooksApi* | [**addWebhook**](docs/WebhooksApi.md#addWebhook) | **POST** /webhook | Add webhook
*QualpayPlatformApi.WebhooksApi* | [**browseWebhook**](docs/WebhooksApi.md#browseWebhook) | **GET** /webhook | Browse webhooks
*QualpayPlatformApi.WebhooksApi* | [**disableWebhook**](docs/WebhooksApi.md#disableWebhook) | **PUT** /webhook/{webhook_id}/disable | Disable a Webhook
*QualpayPlatformApi.WebhooksApi* | [**editWebhook**](docs/WebhooksApi.md#editWebhook) | **PUT** /webhook/{webhook_id} | Update webhook
*QualpayPlatformApi.WebhooksApi* | [**enableWebhook**](docs/WebhooksApi.md#enableWebhook) | **PUT** /webhook/{webhook_id}/enable | Enable a Webhook
*QualpayPlatformApi.WebhooksApi* | [**getEvents**](docs/WebhooksApi.md#getEvents) | **GET** /webhook/{webhook_id}/event | Get events
*QualpayPlatformApi.WebhooksApi* | [**getWebhook**](docs/WebhooksApi.md#getWebhook) | **GET** /webhook/{webhook_id} | Get webhook
*QualpayPlatformApi.WebhooksApi* | [**removeEvent**](docs/WebhooksApi.md#removeEvent) | **DELETE** /webhook/{webhook_id}/event/{event} | Delete event


## Documentation for Models

 - [QualpayPlatformApi.AccountUpdaterReport](docs/AccountUpdaterReport.md)
 - [QualpayPlatformApi.AccountUpdaterResponse](docs/AccountUpdaterResponse.md)
 - [QualpayPlatformApi.AccountUpdaterSummaryReport](docs/AccountUpdaterSummaryReport.md)
 - [QualpayPlatformApi.AccountUpdaterSummaryResponse](docs/AccountUpdaterSummaryResponse.md)
 - [QualpayPlatformApi.AddAppRequest](docs/AddAppRequest.md)
 - [QualpayPlatformApi.AddBillingCardRequest](docs/AddBillingCardRequest.md)
 - [QualpayPlatformApi.AddCustomerRequest](docs/AddCustomerRequest.md)
 - [QualpayPlatformApi.AddRecurringPlanRequest](docs/AddRecurringPlanRequest.md)
 - [QualpayPlatformApi.AddShippingAddressRequest](docs/AddShippingAddressRequest.md)
 - [QualpayPlatformApi.AddSubscriptionRequest](docs/AddSubscriptionRequest.md)
 - [QualpayPlatformApi.ApiKey](docs/ApiKey.md)
 - [QualpayPlatformApi.ApplicationData](docs/ApplicationData.md)
 - [QualpayPlatformApi.ApplicationModel](docs/ApplicationModel.md)
 - [QualpayPlatformApi.ArchiveRecurringPlanRequest](docs/ArchiveRecurringPlanRequest.md)
 - [QualpayPlatformApi.AusRequest](docs/AusRequest.md)
 - [QualpayPlatformApi.AusRequestData](docs/AusRequestData.md)
 - [QualpayPlatformApi.AusRequestId](docs/AusRequestId.md)
 - [QualpayPlatformApi.AusRequestResponse](docs/AusRequestResponse.md)
 - [QualpayPlatformApi.AusResponse](docs/AusResponse.md)
 - [QualpayPlatformApi.AusUpdatedResponse](docs/AusUpdatedResponse.md)
 - [QualpayPlatformApi.BatchReport](docs/BatchReport.md)
 - [QualpayPlatformApi.BatchResponse](docs/BatchResponse.md)
 - [QualpayPlatformApi.BillingCard](docs/BillingCard.md)
 - [QualpayPlatformApi.BouncedInvoice](docs/BouncedInvoice.md)
 - [QualpayPlatformApi.BrowseAppsData](docs/BrowseAppsData.md)
 - [QualpayPlatformApi.BrowseAppsResult](docs/BrowseAppsResult.md)
 - [QualpayPlatformApi.BrowseSalesRepsData](docs/BrowseSalesRepsData.md)
 - [QualpayPlatformApi.BrowseSalesRepsResult](docs/BrowseSalesRepsResult.md)
 - [QualpayPlatformApi.CancelSubscriptionRequest](docs/CancelSubscriptionRequest.md)
 - [QualpayPlatformApi.Checkout](docs/Checkout.md)
 - [QualpayPlatformApi.CheckoutCustomer](docs/CheckoutCustomer.md)
 - [QualpayPlatformApi.CheckoutLink](docs/CheckoutLink.md)
 - [QualpayPlatformApi.CheckoutLinkResponse](docs/CheckoutLinkResponse.md)
 - [QualpayPlatformApi.CheckoutPreferences](docs/CheckoutPreferences.md)
 - [QualpayPlatformApi.CheckoutRequest](docs/CheckoutRequest.md)
 - [QualpayPlatformApi.CheckoutResponse](docs/CheckoutResponse.md)
 - [QualpayPlatformApi.CheckoutSettings](docs/CheckoutSettings.md)
 - [QualpayPlatformApi.Contact](docs/Contact.md)
 - [QualpayPlatformApi.Cookies](docs/Cookies.md)
 - [QualpayPlatformApi.CopyInvoiceRequest](docs/CopyInvoiceRequest.md)
 - [QualpayPlatformApi.CorrespondingTransactionResponse](docs/CorrespondingTransactionResponse.md)
 - [QualpayPlatformApi.CreateInvoiceRequest](docs/CreateInvoiceRequest.md)
 - [QualpayPlatformApi.CustomerListResponse](docs/CustomerListResponse.md)
 - [QualpayPlatformApi.CustomerResponse](docs/CustomerResponse.md)
 - [QualpayPlatformApi.CustomerVault](docs/CustomerVault.md)
 - [QualpayPlatformApi.DeleteBillingCardRequest](docs/DeleteBillingCardRequest.md)
 - [QualpayPlatformApi.DepositReport](docs/DepositReport.md)
 - [QualpayPlatformApi.DepositResponse](docs/DepositResponse.md)
 - [QualpayPlatformApi.DisputeData](docs/DisputeData.md)
 - [QualpayPlatformApi.DisputeDetail](docs/DisputeDetail.md)
 - [QualpayPlatformApi.DisputeReport](docs/DisputeReport.md)
 - [QualpayPlatformApi.DisputeResponse](docs/DisputeResponse.md)
 - [QualpayPlatformApi.DisputeResponseData](docs/DisputeResponseData.md)
 - [QualpayPlatformApi.EmbeddedKey](docs/EmbeddedKey.md)
 - [QualpayPlatformApi.EmbeddedKeyResponse](docs/EmbeddedKeyResponse.md)
 - [QualpayPlatformApi.GatewayResponse](docs/GatewayResponse.md)
 - [QualpayPlatformApi.GetAppData](docs/GetAppData.md)
 - [QualpayPlatformApi.GetAppResult](docs/GetAppResult.md)
 - [QualpayPlatformApi.GetBillingCardsResponse](docs/GetBillingCardsResponse.md)
 - [QualpayPlatformApi.GetBillingResponse](docs/GetBillingResponse.md)
 - [QualpayPlatformApi.GetShippingAddressesResponse](docs/GetShippingAddressesResponse.md)
 - [QualpayPlatformApi.GetShippingResponse](docs/GetShippingResponse.md)
 - [QualpayPlatformApi.HttpEntity](docs/HttpEntity.md)
 - [QualpayPlatformApi.Invoice](docs/Invoice.md)
 - [QualpayPlatformApi.InvoiceBouncedResponse](docs/InvoiceBouncedResponse.md)
 - [QualpayPlatformApi.InvoiceListResponse](docs/InvoiceListResponse.md)
 - [QualpayPlatformApi.InvoicePayment](docs/InvoicePayment.md)
 - [QualpayPlatformApi.InvoicePaymentListResponse](docs/InvoicePaymentListResponse.md)
 - [QualpayPlatformApi.InvoicePaymentRequest](docs/InvoicePaymentRequest.md)
 - [QualpayPlatformApi.InvoicePaymentResponse](docs/InvoicePaymentResponse.md)
 - [QualpayPlatformApi.InvoiceResponse](docs/InvoiceResponse.md)
 - [QualpayPlatformApi.LineItem](docs/LineItem.md)
 - [QualpayPlatformApi.MerchantSettingsResponse](docs/MerchantSettingsResponse.md)
 - [QualpayPlatformApi.NewAccountRequest](docs/NewAccountRequest.md)
 - [QualpayPlatformApi.PGTransactionResponse](docs/PGTransactionResponse.md)
 - [QualpayPlatformApi.PauseSubscriptionRequest](docs/PauseSubscriptionRequest.md)
 - [QualpayPlatformApi.Payment](docs/Payment.md)
 - [QualpayPlatformApi.PaymentProfile](docs/PaymentProfile.md)
 - [QualpayPlatformApi.PaymentTerm](docs/PaymentTerm.md)
 - [QualpayPlatformApi.PlanModel](docs/PlanModel.md)
 - [QualpayPlatformApi.PricingModel](docs/PricingModel.md)
 - [QualpayPlatformApi.QPApiData](docs/QPApiData.md)
 - [QualpayPlatformApi.QPApiListResponse](docs/QPApiListResponse.md)
 - [QualpayPlatformApi.QPApiResponse](docs/QPApiResponse.md)
 - [QualpayPlatformApi.RecurringPlan](docs/RecurringPlan.md)
 - [QualpayPlatformApi.RecurringPlanListResponse](docs/RecurringPlanListResponse.md)
 - [QualpayPlatformApi.RecurringPlanResponse](docs/RecurringPlanResponse.md)
 - [QualpayPlatformApi.ResendInvoiceRequest](docs/ResendInvoiceRequest.md)
 - [QualpayPlatformApi.Result](docs/Result.md)
 - [QualpayPlatformApi.ResumeSubscriptionRequest](docs/ResumeSubscriptionRequest.md)
 - [QualpayPlatformApi.RotateKeyRequest](docs/RotateKeyRequest.md)
 - [QualpayPlatformApi.RotateKeyResponse](docs/RotateKeyResponse.md)
 - [QualpayPlatformApi.SalesTax](docs/SalesTax.md)
 - [QualpayPlatformApi.SaveDataRequest](docs/SaveDataRequest.md)
 - [QualpayPlatformApi.SendInvoiceRequest](docs/SendInvoiceRequest.md)
 - [QualpayPlatformApi.SetPrimaryBillingCardRequest](docs/SetPrimaryBillingCardRequest.md)
 - [QualpayPlatformApi.SetPrimaryShippingAddressRequest](docs/SetPrimaryShippingAddressRequest.md)
 - [QualpayPlatformApi.Settings](docs/Settings.md)
 - [QualpayPlatformApi.SettledTransactionReport](docs/SettledTransactionReport.md)
 - [QualpayPlatformApi.SettledTransactionResponse](docs/SettledTransactionResponse.md)
 - [QualpayPlatformApi.ShippingAddress](docs/ShippingAddress.md)
 - [QualpayPlatformApi.Subscription](docs/Subscription.md)
 - [QualpayPlatformApi.SubscriptionListResponse](docs/SubscriptionListResponse.md)
 - [QualpayPlatformApi.SubscriptionResponse](docs/SubscriptionResponse.md)
 - [QualpayPlatformApi.Transaction](docs/Transaction.md)
 - [QualpayPlatformApi.TransactionData](docs/TransactionData.md)
 - [QualpayPlatformApi.TransactionListResponse](docs/TransactionListResponse.md)
 - [QualpayPlatformApi.TransactionRequestReport](docs/TransactionRequestReport.md)
 - [QualpayPlatformApi.TransactionRequestResponse](docs/TransactionRequestResponse.md)
 - [QualpayPlatformApi.UpdateBillingCardRequest](docs/UpdateBillingCardRequest.md)
 - [QualpayPlatformApi.UpdateCustomerRequest](docs/UpdateCustomerRequest.md)
 - [QualpayPlatformApi.UpdateDraftRequest](docs/UpdateDraftRequest.md)
 - [QualpayPlatformApi.UpdateOutstandingRequest](docs/UpdateOutstandingRequest.md)
 - [QualpayPlatformApi.UpdateRecurringPlanRequest](docs/UpdateRecurringPlanRequest.md)
 - [QualpayPlatformApi.UpdateShippingAddressRequest](docs/UpdateShippingAddressRequest.md)
 - [QualpayPlatformApi.UpdateSubscriptionRequest](docs/UpdateSubscriptionRequest.md)
 - [QualpayPlatformApi.Webhook](docs/Webhook.md)
 - [QualpayPlatformApi.WebhookAuthSetting](docs/WebhookAuthSetting.md)
 - [QualpayPlatformApi.WebhookEvent](docs/WebhookEvent.md)
 - [QualpayPlatformApi.WebhookEventResponse](docs/WebhookEventResponse.md)
 - [QualpayPlatformApi.WebhookResponse](docs/WebhookResponse.md)
 - [QualpayPlatformApi.WebhooksListResponse](docs/WebhooksListResponse.md)


## Documentation for Authorization


### basicAuth

- **Type**: HTTP basic authentication

