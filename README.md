The purpose of this sample app is to demonstrate the usage of Qualpay Embedded fields using NodeJS.

**Dependencies**

You will need

	•	A qualpay sandbox account. Create your sandbox account at https://api-test.qualpay.com.
	•	Ensure that you have an api security key and the key has access Payment Gateway API and Embedded Fields API. Refer to https://www.qualpay.com/developer/api/testing#security-keys.
	•	NodeJS.
	•	Chrome or Firefox browser. The application has been tested on latest version of Chrome and Firefox.
	
**Configuration**

Update config/confi.json

	•	Update merchant_id to point to your merchant account id.
	•	Udpate security_key to point to your merchant api security_key.
	•	Update any other property as appropriate.

**Code samples**

	•	index.html - Use Embedded fields to capture card data and use it to invoke a sale request.
    •	api/controllers/embeddedController.js - Get transient key for use with embedded fiels
	•	api/controllers/sale.js - Use Payment gateway to make a payment using card id

**Qualpay SDKs**

The application uses the Platform and Payment Gateway Javascript SDKs to access the Qualpay Platform and Payment Gateway REST APis. 

SDK set up

	•	Access the Qualpay SDK download page https://www.qualpay.com/developer/libs/sdk. 
	•	Choose the appropriate SDK tab - Payment Gateway or Platform.
	•	Remove the “invokePackage” property from the SDK Options (Click on the Show SDK Options Input) on the download page. Below is a sample. 
					{"packageName": "qpPlatform","hideGenerationTimestamp": true}
	•	Click on the Javascript link to download the SDK.
	•	Copy the SDK to your project folder, this application, have it installed under vendor/qp-pg-api & vendor/qp-platform-api.
	•	Refer to the README.md in the SDK to use the libraries locally. 
 
While installing the Payment Gateway Javascript SDK, npm install may complain that the version is invalid. This is a known issue and will be resolved soon. In the interim, to fix this issue, open the payment gateway package.json file and change the version from "1.7" to "1.7.0"
